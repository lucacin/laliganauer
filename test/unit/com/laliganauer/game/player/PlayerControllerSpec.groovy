package com.laliganauer.game.player

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError
import spock.lang.Specification
import com.grailsinaction.LameSecurityFilters
import com.laliganauer.game.player.Player
import com.laliganauer.game.player.PlayerService
import com.laliganauer.game.player.ForgotPasswordCommand
import com.laliganauer.game.player.LoginCommand
import com.laliganauer.game.player.PlayerController
import com.laliganauer.game.player.RegisterCommand
import grails.test.GrailsMock
import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import grails.validation.ValidationException
import org.apache.commons.logging.Log
import com.laliganauer.utils.Utils


// automatically injects a controller
@TestFor(PlayerController)
@Mock(LameSecurityFilters)
class PlayerControllerSpec extends Specification {



	def "register:  Register a new player succesfully" (){

		given: "Register command correct data"

		def registerCommand = new RegisterCommand (login:"garibolo", email:"garibolo@gmail.com", password:"garibolo_16",confirmPassword:"garibolo_16")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		def mockPlayerLog = Mock(Log)
		// mockPlayerService.register is mocked to return Player
		def mockPlayer = new Player(registerCommand.properties)
		mockPlayerService.register(_) >> mockPlayer

		controller.playerService = mockPlayerService
		controller.log = mockPlayerLog

		when: "Register"

		withFilters (controller:'player', action:'register') { controller.register(registerCommand)}

		then: "Show OK message and redirect to login"

		// mockPlayerService.register is executed only once
		1 * mockPlayerService.register(_) >> mockPlayer
		1 * mockPlayerLog.info (_)
		
		flash.OKMessage == controller.message(code: "register.OK", args: ["${mockPlayer.login}"])
		flash.KOMessage == null
		flash.NotificationMessage == null
		response.redirectedUrl == "/"
	}

	def "register: Error validating new player" (){

		given: "Register command incorrect data"
		def registerCommand = new RegisterCommand (login:"garibolo", email:"garibolo@gmail.com", password:"garibolo_16",confirmPassword:"garibolo_16")
		registerCommand.errors.rejectValue("login","field.code.error")
	
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		
		// mockPlayerService.register is mocked to return PlayerRegisterException				
		def mockValidationException = new ValidationException("Mock validation Exception",registerCommand.errors)
		mockPlayerService.register(_) >> { throw mockValidationException }
		
		GroovyMock(Utils, global: true)				 		
		
		controller.playerService = mockPlayerService
		

		when: "Register"

		withFilters (controller:'player',action:'register') { controller.register(registerCommand)}

		then: "Show again the register form"
		
		//1 * = executed only once
		1 * mockPlayerService.register(_) >> { throw mockValidationException }
		1 * Utils.logErrors(_,_,_)
		response.redirectedUrl == null
	}


	def "register: Error saving new player" (){

		given: "Player command incorrect saving data"
		def registerCommand = new RegisterCommand (login:"garibolo", email:"garibolo@gmail.com", password:"garibolo_16",confirmPassword:"garibolo_16")		
		registerCommand.errors.reject('mock.code',"Mock default message" )
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.register is executed only once and it is mocked to return PlayerException
		def mockValidationException = new ValidationException("Mock validation Exception",registerCommand.errors)
		mockPlayerService.register(_) >> { throw mockValidationException }		
		GroovyMock(Utils, global: true)
		
		controller.playerService = mockPlayerService

		when: "Register"
		withFilters (controller:'player',action:'register') { controller.register(registerCommand) }

		then: "Show again the register form"

		1 * mockPlayerService.register(_) >> { throw mockValidationException }
		1 * Utils.logErrors(_,_,_)
		response.redirectedUrl == null
	}

	def "register (filter): KO authenticated access"() {
		given: "Authentication access"
		def registerCommand = new RegisterCommand (login:"garibolo", email:"garibolo@gmail.com", password:"garibolo_16",confirmPassword:"garibolo_16")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.register is not executed because controller.register is not executed either due to the filter
		def mockPlayer = new Player(registerCommand.properties)
		mockPlayerService.register(_) >> mockPlayer
		controller.playerService = mockPlayerService

		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when:
		withFilters (controller:'player',action:'register') { controller.register(registerCommand) }

		then:
		0 * mockPlayerService.register(_) >> mockPlayer
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl == '/player/home'
	}

	def "login: Login succesfully"() {
		given: "Registered player"

		def loginCommand = new LoginCommand (login:"garibolo", password:"garibolo.16")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.login is executed only once and it is mocked to return Player
		def mockPlayerLog = Mock(Log)
		def mockPlayer = new Player(loginCommand.properties)
		mockPlayerService.login(_) >> mockPlayer
		controller.playerService = mockPlayerService
		controller.log = mockPlayerLog

		when: "Login"

		withFilters (controller:'player',action:'login'){controller.login(loginCommand)}

		then: "User is logged in"
		1 * mockPlayerService.login(_) >> mockPlayer
		1 * mockPlayerLog.info (_)
		def securityFilters = new LameSecurityFilters()
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl =='/player/home'
	}

	def "login: Login error"() {
		given: "Not registered player"

		def loginCommand = new LoginCommand (login:"garibolo", password:"garibolo.16")		
		loginCommand.errors.reject('mock.code',"Mock default message" )
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.login is executed only once and it is mocked to return mockPlayer		
		def validationException = new ValidationException("Mock validation Exception",loginCommand.errors)		
		mockPlayerService.login(_) >> { throw validationException }		
		GroovyMock(Utils, global: true)
		
		controller.playerService = mockPlayerService

		when: "Login"

		withFilters (controller:'player',action:'login') {controller.login(loginCommand)}

		then: "Show the login form again"
		1 * Utils.logErrors(_,_,_)
		1 * mockPlayerService.login(_) >> { throw validationException }
		response.redirectedUrl == null
	}
	def "login (filter): KO, authenticated user"() {


		given: "Authentication access"

		def loginCommand = new LoginCommand (login:"offside", password:"offside_12")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.login is not executed due to the filter
		def mockPlayer = new Player(loginCommand.properties)
		mockPlayerService.login(_) >> mockPlayer
		controller.playerService = mockPlayerService

		// Authenticated access
		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when:
		withFilters (controller:'player',action:'login') { controller.login(loginCommand) }

		then:
		0 * mockPlayerService.login(_) >> mockPlayer
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl == '/player/home'
	}

	def "forgotPassword: forgotPassword by login OK"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"garibolo", email:"", password:"garibolo_16",confirmPassword:"garibolo_16")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		def mockPlayerLog = Mock(Log)
		// mockPlayerService.forgotPasswordByLogin is executed only once and it is mocked to return mockPlayer
		def mockPlayer = new Player(forgotPasswordCommand.properties)
		mockPlayer.email="garibolo@gmail.com"
		params.forgotPasswordSelected="login"
		mockPlayerService.forgotPasswordByLogin(_) >> mockPlayer
		
		controller.playerService = mockPlayerService
		controller.log  =mockPlayerLog
		
		when:	"forgotPassword"

		withFilters (controller:'player',action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"redirected lo login"
		1 * mockPlayerService.forgotPasswordByLogin(_) >> mockPlayer
		1 * mockPlayerLog.info (_)
		flash.OKMessage == controller.message(code: "forgotPassword.OK", args: [mockPlayer.login])
		flash.KOMessage == null
		flash.NotificationMessage == null
		response.redirectedUrl == '/'
	}

	def "forgotPassword: forgotPassword by email OK"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"", email:"garibolo@gmail.com", password:"garibolo_16",confirmPassword:"garibolo_16")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		def mockPlayerLog = Mock(Log)
		// mockPlayerService.forgotPasswordByEmail is executed only once and it is mocked to return mockPlayer
		def mockPlayer = new Player(forgotPasswordCommand.properties)
		mockPlayer.login="garibolo"
		params.forgotPasswordSelected="email"
		mockPlayerService.forgotPasswordByEmail(_) >> mockPlayer
		controller.playerService = mockPlayerService
		controller.log = mockPlayerLog
		
		when :	"forgotPassword"

		withFilters (controller:'player',action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"redirected lo login"
		1 * mockPlayerService.forgotPasswordByEmail(_) >> mockPlayer
		1 * mockPlayerLog.info (_)
		flash.OKMessage == controller.message(code: "forgotPassword.OK", args: [mockPlayer.login])
		flash.KOMessage == null
		flash.NotificationMessage == null

		response.redirectedUrl == '/'
	}

	def "forgotPassword: forgotPassword by login errors"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"", email:"", password:"garibolo_16",confirmPassword:"garibolo_16")

		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		
		forgotPasswordCommand.errors.rejectValue("login","blank")
		// mockPlayerService.forgotPasswordByLogin is executed only once and it is mocked to return PlayerForgotPasswordException
		def mockValidationException = new ValidationException("Mock validation Exception",forgotPasswordCommand.errors)
		params.forgotPasswordSelected="login"
		
		GroovyMock(Utils, global: true)		
		mockPlayerService.forgotPasswordByLogin(_) >> { throw mockValidationException}
		controller.playerService = mockPlayerService
		

		when :	"forgotPassword"

		withFilters (controller:'player',action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"not redirected"

		1 * mockPlayerService.forgotPasswordByLogin(_) >> { throw mockValidationException}
		1 * Utils.logErrors(_,_,_)
		response.redirectedUrl == null
	}

	def "forgotPassword: forgotPassword by email errors"() {
		given: "Forgot password command"
		def forgotPasswordCommand = new ForgotPasswordCommand (login:"", email:"", password:"garibolo_16",confirmPassword:"garibolo_16")
		forgotPasswordCommand.errors.rejectValue("email","blank")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByLogin is executed only once and it is mocked to return PlayerForgotPasswordException
		def mockValidationException = new ValidationException("Mock validation Exception",forgotPasswordCommand.errors)
		params.forgotPasswordSelected="email"
		GroovyMock(Utils, global: true)
		
		mockPlayerService.forgotPasswordByEmail(_) >> { throw mockValidationException}
		controller.playerService = mockPlayerService

		when :	"forgotPassword"

		withFilters (controller:'player',action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"not redirected"
		1 * mockPlayerService.forgotPasswordByEmail(_) >> { throw mockValidationException}
		1 * Utils.logErrors(_,_,_)
		response.redirectedUrl == null
	}

	def "forgotPassword (filter): KO, authenticated users"() {
		given: "Authenticated access"

		def forgotPasswordCommand = new ForgotPasswordCommand (login:"garibolo", email:"", password:"garibolo.16",confirmPassword:"garibolo.16")
		// Mock service to test Controller in isolation
		def mockPlayerService = Mock(PlayerService)
		// mockPlayerService.forgotPasswordByLogin is not executed due to the filter
		def mockPlayer = new Player(forgotPasswordCommand.properties)
		mockPlayer.email="garibolo@gmail.com"
		params.forgotPasswordSelected="login"
		mockPlayerService.forgotPasswordByLogin(_) >> mockPlayer
		mockPlayerService.forgotPasswordByEmail(_) >> mockPlayer
		controller.playerService = mockPlayerService

		// Authenticated access
		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)

		when :	"forgotPassword"

		withFilters (controller:'player',action:'forgotPassword') {controller.forgotPassword(forgotPasswordCommand)}

		then:"redirected lo login"
		0 * mockPlayerService.forgotPasswordByLogin(_) >> mockPlayer
		0 * mockPlayerService.forgotPasswordByEmail(_) >> mockPlayer
		securityFilters.isAuthenticatedAccess(session) == true
		response.redirectedUrl == '/player/home'
	}


	def "logout: OK"() {
		given: "Authenticated access"
		def mockPlayer = new Player (login:"garibolo", email:"garibolo@gmail.com")
		def mockPlayerLog = Mock(Log)
		def securityFilters = new LameSecurityFilters()
		securityFilters.setAuthentication(session,mockPlayer)
		controller.log = mockPlayerLog 
		when :	"logout"
		withFilters (controller:'player',action:'logout') {controller.logout()}

		then:"redirected"

		1 * mockPlayerLog.info (_)
		securityFilters.isAuthenticatedAccess(session) == false
		flash.OKMessage == controller.message(code: "player.logout.ok", args: [mockPlayer.login])
		flash.KOMessage == null
		flash.NotificationMessage == null
		response.redirectedUrl == '/'

	}

	def "logout (filter): KO, unauthenticated access"() {
		given: "Unauthenticated access"
		def mockPlayer = new Player (login:"garibolo", email:"garibolo@gmail.com")
		def securityFilters = new LameSecurityFilters()
		
		when :	"logout"
		withFilters (controller:'player',action:'logout') { controller.logout()}

		then:"redirected"

		securityFilters.isAuthenticatedAccess(session) == false
		flash.OKMessage == null
		flash.KOMessage == controller.message(code: "error.notauthenticated.player")
		flash.NotificationMessage == null
		response.redirectedUrl == '/'

	}


}
