package com.laliganauer.game.tournament



import grails.test.mixin.*
import grails.validation.ValidationException
import groovy.util.logging.Log
import com.grailsinaction.LameSecurityFilters
import com.laliganauer.game.tournament.Tournament
import com.laliganauer.game.tournament.TournamentService
import com.laliganauer.game.tournament.CreateTournamentCommand
import com.laliganauer.game.tournament.TournamentController
import com.laliganauer.game.player.Player

import spock.lang.Specification
import org.apache.commons.logging.Log
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession
import org.springframework.validation.AbstractBindingResult
import org.springframework.validation.AbstractErrors
import org.springframework.validation.Errors
import org.springframework.validation.FieldError
import org.springframework.validation.MapBindingResult

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(TournamentController)
@Mock(LameSecurityFilters)
class TournamentControllerSpec extends Specification{

	def "CreateTournament:  Create a new tournament succesfully" (){

		given: "Tournament command correct data"

		//Authenticated player
		def securityFilters = new LameSecurityFilters()
		def mockPlayer = new Player (login:"offside", email:"offside.dev@gmail.com", passwordHash:"passwordHashed")
		securityFilters.setAuthentication(session,mockPlayer)

		
		def createTournamentCommand = new CreateTournamentCommand (name:"La Liga Nauer", description:"Liga description")
		// Mock service to test Controller in isolation
		def mockTournamentService = Mock(TournamentService)
		controller.tournamentService = mockTournamentService
		def mockTournament = new Tournament(name: 'name', reference: 'reference',description: 'description',status:TournamentStatus.OPENED)
		mockTournament.setRandomId()
				
		// mockTournamentService.create is executed only once and it is mocked to return mockTournament
		1 * mockTournamentService.createTournamentAndAssociatedTeam(_,_,_) >> mockTournament
		
		when: "Create Tournament"

		withFilters (controller:'tournament',action:'create') {controller.create(createTournamentCommand)}


		then: "Show OK message and redirect to tournaments"
		flash.KOMessage == null
		flash.NotificationMessage == null
		flash.OKMessage == controller.message(code: "tournament.createTournament.ok", args: [
			mockTournament.name,
			mockTournament.reference
		])
		response.redirectedUrl == "/tournament/listByPlayer"
	}

	def "CreateTournament: Error validating new Tournament" (){


		/*			given: "Register command incorrect data"
		 def registerCommand = new RegisterCommand (login:"offside", email:"offside.dev@gmail.com", password:"offside_12",confirmPassword:"offside_12")
		 registerCommand.errors.reject('mock.code',"Mock default message" )
		 // Mock service to test Controller in isolation
		 def mockPlayerService = Mock(PlayerService)
		 // mockPlayerService.register is executed only once and it is mocked to return PlayerRegisterException
		 def mockRegisterException = new RegisterCommandException(message:"Mock RegisterCommandException", registerCommand: registerCommand)
		 1 * mockPlayerService.register(_) >> { throw mockRegisterException }
		 controller.playerService = mockPlayerService
		 when: "Register"
		 withFilters (controller:'player',action:'register') { controller.register(registerCommand)}
		 then: "Show again the register form"
		 response.redirectedUrl == null
		 */


		given: "Tournament command incorrect data"

		// Authenticated player
		def securityFilters = new LameSecurityFilters()
		def mockPlayer = new Player (login:"offside", email:"offside.dev@gmail.com", passwordHash:"passwordHashed")
		securityFilters.setAuthentication(session,mockPlayer)
		

		def createTournamentCommand = new CreateTournamentCommand (name:"La Liga Nauer", description:"Liga description")
		createTournamentCommand.errors.reject('mock.code',"Mock default message" )
		
		// Mock service to test Controller in isolation
		def mockTournamentService = Mock(TournamentService)
		def mockLog = Mock(Log)
		//def mockValidationException = Mock(ValidationException,constructorArgs: ["Mock ValidationException",createTournamentCommand.errors])

		def mockValidationException = new ValidationException("Mock ValidationException",createTournamentCommand.errors)
		controller.tournamentService = mockTournamentService		
		controller.log = mockLog
															
		1 * mockTournamentService.createTournamentAndAssociatedTeam(_,_,_) >> { throw mockValidationException }
		1 * mockValidationException.errors.hasGlobalErrors() 
		1 * mockLog.error (_,_)		
		
		when: "Create Tournament"

		withFilters (controller:'tournament',action:'create') {controller.create(createTournamentCommand)}
		

		/*then: "Show OK message and redirect to tournaments"
		flash.KOMessage == null
		flash.NotificationMessage == null
		flash.OKMessage == null
		response.redirectedUrl == null*/
		then:
		//mockValidationException.errors.hasFieldErrors() == false
		//mockValidationException.errors.hasGlobalErrors() == true
		response.redirectedUrl == null
				
	}

}
