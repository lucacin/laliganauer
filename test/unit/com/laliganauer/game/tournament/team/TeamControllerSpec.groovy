package com.laliganauer.game.tournament.team



import grails.test.mixin.*
import org.junit.*

import com.laliganauer.game.tournament.team.TeamController

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(TeamController)
class TeamControllerSpec {

    void testSomething() {
       fail "Implement me"
    }
}
