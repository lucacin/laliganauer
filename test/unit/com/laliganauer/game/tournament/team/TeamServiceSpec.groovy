package com.laliganauer.game.tournament.team



import grails.test.mixin.*
import org.junit.*

import com.laliganauer.game.tournament.team.TeamService;

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TeamService)
class TeamServiceSpec {

    void testSomething() {
        fail "Implement me"
    }
}
