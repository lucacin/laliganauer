package com.laliganauer.game.player

import grails.validation.ValidationException

import spock.lang.*
import grails.plugin.spock.*
import spock.lang.Specification
import grails.test.mixin.TestFor
import grails.test.mixin.Mock


// TODO : validate error messages (no se si deberia hacerse cuando probamos el service, supongo que si)
class PlayerServiceIntegrationSpec extends IntegrationSpec {

	def "PlayerService.register:  Register a new player succesfully" (){

		def playerService = new PlayerService()

		given: "Register command correct data"

		def registerCommand = new RegisterCommand (login:"garibolo", email:"garibolo@gmail.com", password:"garibolo_16",confirmPassword:"garibolo_16")

		when: "Register"

		def player=playerService.register(registerCommand)

		then: "Registered in DB"

		player.login==registerCommand.login
		player.email == registerCommand.email
		player.checkPassword(registerCommand.password) == true
		player.profile == null

		Player.findByLogin(registerCommand.login)==player
	}

	@Unroll
	def "PlayerService.register:  Constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Register command non validatable objects"

		def playerService = new PlayerService()

		def registerCommand = new RegisterCommand (login:login,email:email,password:password,confirmPassword:confirmPassword)

		when:"Register"

		def player=playerService.register(registerCommand)
		

		then:
		
		ValidationException validationException = thrown()

		def errors = registerCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode


		where:
		field|login|email|password|confirmPassword|exceptionCode
		'login'|null|'garibolo@gmail.com'|'garibolo'|'garibolo'|'nullable'
		'login'|''|'garibolo@gmail.com'|'garibolo'|'garibolo'|'blank'
		'login'|'gar'|'garibolo@gmail.com'|'garibolo'|'garibolo'|'size.toosmall'
		'login'|'garibolo012345'|'garibolo@gmail.com'|'garibolo'|'garibolo'|'size.toobig'
		'email'|'garibolo'|null|'garibolo'|'garibolo'|'nullable'
		'email'|'garibolo'|''|'garibolo'|'garibolo'|'blank'
		'email'|'garibolo'|'garibolo@gmail'|'garibolo'|'garibolo'|'email.invalid'
		'password'|'garibolo'|'garibolo@gmail.com'|null|'garibolo'|'nullable'
		'password'|'garibolo'|'garibolo@gmail.com'|''|'garibolo'|'blank'
		'password'|'garibolo'|'garibolo@gmail.com'|'gari'|'garibolo'|'size.toosmall'
		'password'|'garibolo'|'garibolo@gmail.com'|'garibolo12345'|'garibolo'|'size.toobig'
		'confirmPassword'|'garibolo'|'garibolo@gmail.com'|'garibolo'|null|'nullable'
		'confirmPassword'|'garibolo'|'garibolo@gmail.com'|'garibolo'|''|'blank'
		'confirmPassword'|'garibolo'|'garibolo@gmail.com'|'garibolo'|'GARIBOLO'|'validator.invalid'
	}

	@Unroll
	def "PlayerService.register:  Unique validations: field: #field, constraint: #exceptionCode"() {
		given: "Register command non validatable objects"

		def playerService = new PlayerService()
		def password = 'garibolo2'

		def registerCommand = new RegisterCommand (login:login,email:email,password:password,confirmPassword: password)

		def registerCommand2 = new RegisterCommand (login:login2,email:email2,password:password,confirmPassword: password)

		when:"Register"

		def player=playerService.register(registerCommand)
		def player2=playerService.register(registerCommand2)

		then:
		ValidationException validationException = thrown()

		def errors = validationException.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode

		
		where:
		field|login|email|login2|email2|exceptionCode
		'login'|'garibolo'|'garibolo@gmail.com'|'garibolo'|'laliganauer@gmail.com'|'unique'
		'email'|'garibolo'|'garibolo@gmail.com'|'laliganauer'|'garibolo@gmail.com'|'unique'
	}

	def "PlayerService.login:  Login succesfully" (){

		def playerService = new PlayerService()

		given: "Login command correct data"
		def login = 'garibolo'
		def password = 'garibolo.16'

		def registerCommand = new RegisterCommand (login:login, email:'garibolo@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login:login, password: password)

		when: "Register and then login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login validates player"

		player.login==loginCommand.login
		player.checkPassword(loginCommand.password) == true
	}

	@Unroll
	def "PlayerService.login:  Constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Login command non validatable objects"

		def playerService = new PlayerService()

		def loginCommand = new LoginCommand (login: login,password: password)

		when:"login"

		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		ValidationException validationException = thrown()

		def errors = loginCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode


		where:
		field|login|password|exceptionCode
		'login'|null|'garibolo_16'|'nullable'
		'login'|''|'garibolo_16'|'blank'
		'login'|'gar'|'garibolo_16'|'size.toosmall'
		'login'|'garibolo_0123'|'garibolo.com'|'size.toobig'
		'password'|'garibolo'|null|'nullable'
		'password'|'garibolo'|''|'blank'
		'password'|'garibolo'|'garib'|'size.toosmall'
		'password'|'garibolo'|'garibolo_0123'|'size.toobig'
	}


	def "PlayerService.login:  Player not found"() {
		given: "Incorrect login field in login command"

		def playerService = new PlayerService()
		String login = 'garibolo'
		String login2 = 'noplayer'
		def password = 'garibolo'
		def field = "login"

		def registerCommand = new RegisterCommand (login:login, email:'garibolo@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login: login2,password: password)

		when:"login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		ValidationException validationException = thrown()

		def errors = loginCommand.errors

		errors.hasGlobalErrors() == false
		errors.getErrorCount() == 1
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == 'com.laliganauer.game.player.LoginCommand.login.not.found'
	}

	def "PlayerService.login:  Incorrect password"() {
		given: "Incorrect password"

		def playerService = new PlayerService()
		String login = 'garibolo'
		def password = 'garibolo'
		def password2 = 'nopasswd'
		def field ="password"

		def registerCommand = new RegisterCommand (login:login, email:'garibolo@gmail.com', password:password,confirmPassword:password)
		def loginCommand = new LoginCommand (login: login,password: password2)

		when:"login"

		playerService.register(registerCommand)
		def player=playerService.login(loginCommand)

		then: "Login player validate errors"
		ValidationException validationException = thrown()

		def errors = loginCommand.errors
		
		errors.hasGlobalErrors() == false
		errors.getErrorCount() == 1
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == 'com.laliganauer.game.player.LoginCommand.password.incorrect'
	}


	def "PlayerService.forgotPasswordByLogin success"() {
		given: "Correct forgotPassword data"

		def playerService = new PlayerService()

		def login = ' garibolo'
		def password = 'garibolo.12'
		def newPassword = 'Canario'

		def registerCommand = new RegisterCommand (login:login, email:'garibolo@gmail.com', password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(login: login,password: newPassword,confirmPassword:newPassword)


		when: "set a new password"

		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByLogin(forgotPasswordCommand)

		then: "password is changed"

		player.login == registerCommand.login
		player.checkPassword(newPassword) == true
		
		Player playerFound = Player.findByLogin(login)
		playerFound.checkPassword(newPassword) == true
	}

	@Unroll
	def "PlayerService.forgotPasswordByLogin constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def loginRegister = 'garibolo'
		def passwordRegister = 'garibolo.16'
		def registerCommand = new RegisterCommand (login:loginRegister, email:'garibolo@gmail.com', password:passwordRegister,confirmPassword:passwordRegister)
		def forgotPasswordCommand = new ForgotPasswordCommand(login: login,password: newPassword,confirmPassword:newConfirmPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByLogin(forgotPasswordCommand)

		then: "Exception is thrown and password is not changed"
		
		ValidationException validationException = thrown()

		def errors = forgotPasswordCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode

		Player playerFound = Player.findByLogin(loginRegister)
		playerFound.checkPassword(passwordRegister) == true
		
		where:
		field|login|newPassword|newConfirmPassword|exceptionCode
		'login'|null|'garibolo12'|'garibolo12'|'nullable'
		'login'|''|'garibolo12'|'garibolo12'|'blank'
		'login'|'gar'|'offsid12'|'garibolo12'|'size.toosmall'
		'login'|'garibolo12345'|'offside12'|'offside12'|'size.toobig'
		'password'|'garibolo'|null|'garibolo12'|'nullable'
		'password'|'garibolo'|''|'garibolo12'|'blank'
		'password'|'garibolo'|'gar'|'garibolo12'|'size.toosmall'
		'password'|'garibolo'|'garibolo12345'|'garibolo12'|'size.toobig'
		'confirmPassword'|'garibolo'|'garibolo12'|null|'nullable'
		'confirmPassword'|'garibolo'|'garibolo12'|''|'blank'
		'confirmPassword'|'garibolo'|'garibolo12'|'Garibolo12'|'validator.invalid'
	}

	def "PlayerService.forgotPasswordByLogin : Player not found"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def login = 'garibolo'
		def otherLogin ="different"
		def password = 'garibol_16'
		def newPassword = 'g4r1b0l0'
		def field="login"

		def registerCommand = new RegisterCommand (login:login, email:'garibolo@gmail.com', password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(login: otherLogin,password: newPassword,confirmPassword:newPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByLogin(forgotPasswordCommand)

		then: "Exception is thrown"
		ValidationException validationException = thrown()

		def errors = forgotPasswordCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == 'com.laliganauer.game.player.ForgotPasswordCommand.login.not.found'
		
	}


	def "PlayerService.forgotPasswordByEmail success"() {
		given: "Correct forgotPassword data"

		def playerService = new PlayerService()

		def login = ' garibolo'
		def email = 'garibolo@gmail.com'
		def password = 'garibolo.12'
		def newPassword = 'g4r1b0l0'

		def registerCommand = new RegisterCommand (login:login, email:email, password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(email: email,password: newPassword,confirmPassword:newPassword)


		when: "set a new password"

		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByEmail(forgotPasswordCommand)

		then: "password is changed"

		player.login == registerCommand.login
		player.email==forgotPasswordCommand.email
		player.checkPassword(newPassword) == true
		
		Player playerFound = Player.findByEmail(email)
		playerFound.checkPassword(newPassword) == true
		
	}

	@Unroll
	def "PlayerService.forgotPasswordByEmail constraint validations: field: #field, constraint: #exceptionCode"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def loginRegister = 'garibolo'
		def emailRegister = 'garibolo@gmail.com'
		def passwordRegister = 'garibolo_16'

		def registerCommand = new RegisterCommand (login:loginRegister, email:emailRegister, password:passwordRegister,confirmPassword:passwordRegister)
		def forgotPasswordCommand = new ForgotPasswordCommand(email: email,password: newPassword,confirmPassword:newConfirmPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByEmail(forgotPasswordCommand)

		then: "Exception is thrown and password is not changed"
		ValidationException validationException = thrown()

		def errors = forgotPasswordCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == exceptionCode

		Player playerFound = Player.findByEmail(emailRegister)
		playerFound.checkPassword(passwordRegister) == true

		where:
		field|email|newPassword|newConfirmPassword|exceptionCode
		'email'|null|'garibolo_16'|'garibolo_16'|'nullable'
		'email'|''|'garibolo_16'|'garibolo_16'|'blank'
		'email'|'garibolo@gmail'|'garibolo_16'|'garibolo_16'|'email.invalid'
		'password'|'garibolo@gmail.com'|null|'garibolo_16'|'nullable'
		'password'|'garibolo@gmail.com'|''|'garibolo_16'|'blank'
		'password'|'garibolo@gmail.com'|'garib'|'garibolo_16'|'size.toosmall'
		'password'|'garibolo@gmail.com'|'garibolo01234'|'garibolo_16'|'size.toobig'
		'confirmPassword'|'garibolo@gmail.com'|'garibolo_16'|null|'nullable'
		'confirmPassword'|'garibolo@gmail.com'|'garibolo_16'|''|'blank'
		'confirmPassword'|'garibolo@gmail.com'|'garibolo_16'|'Garibolo_16'|'validator.invalid'		
		
	}

	def "PlayerService.forgotPasswordByEmail : Player not found"() {
		given: "Incorrect forgotPassword data"

		def playerService = new PlayerService()
		def login = 'garibolo'
		def email = 'garibolo@gmail.com'
		def otherEmail = 'different@gmail.com'
		def password = 'garibolo_16'
		def newPassword = 'g4r1b0l0'
		def field ="email"

		def registerCommand = new RegisterCommand (login:login, email:email, password:password,confirmPassword:password)
		def forgotPasswordCommand = new ForgotPasswordCommand(email: otherEmail,password: newPassword,confirmPassword:newPassword)

		when: "try to set a new password"
		playerService.register(registerCommand)
		def player = playerService.forgotPasswordByEmail(forgotPasswordCommand)

		then: "Exception is thrown"
		ValidationException validationException = thrown()

		def errors = forgotPasswordCommand.errors
		errors.hasFieldErrors(field) == true
		errors.getFieldErrorCount(field) == 1
		errors.getFieldError(field).getCode() == 'com.laliganauer.game.player.ForgotPasswordCommand.email.not.found'

	}




}
