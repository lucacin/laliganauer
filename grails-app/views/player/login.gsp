<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="layout" content="main">
<title><g:message code="login.tab.title" /></title>
</head>
<body id="body">
	<h1>
		<g:message code="login.promo.game" />
	</h1>
	<p>
		<g:message code="login.desc.game" />
	</p>
	<div id="notificationsFlashPane">
		<g:if test="${flash.OKMessage}">
			<div id="notificationOKPane" style="background-color: lightgreen;">
				${flash.OKMessage}
			</div>
		</g:if>

		<g:if test="${flash.KOMessage}">
			<div id="notificationKOPane" style="background-color: red;">
				${flash.KOMessage}
			</div>
		</g:if>

		<g:if test="${flash.infoMessage}">
			<div id="notificationInfoPane" style="background-color: blue;">
				${flash.infoMessage}
			</div>
		</g:if>
	</div>

	<!-- Login errors -->
	<g:hasErrors>
		<div id="errorsPane" style="background-color: red;">
			<g:renderErrors bean="${playerView}" as="list" />
		</div>
	</g:hasErrors>

	<div id="loginPane">
		<g:form name="loginForm" controller="player" action="login">
			<div class="formField">
				<label for="login"><g:message
						code="login.form.label.login" /> </label>
				<g:textField name="login" value="${playerView?.login}" />
			</div>
			<div class="formField">
				<label for="password"><g:message
						code="login.form.label.password" /> </label>
				<g:passwordField name="password" />
			</div>
			<div class="formField">
				<g:submitButton name="loginBtn" value="Login" />
			</div>
		</g:form>
	</div>

	<div id="registerPane">
		<g:link controller="player" action="register">
			<g:message code="login.link.register" />
		</g:link>
	</div>
	<div id="forgotPane">
		<g:link controller="player" action="forgotPassword">
			<g:message code="login.link.forgot.password" />
		</g:link> 
	</div>


</body>
</html>