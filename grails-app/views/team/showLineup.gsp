<%@ page import="com.laliganauer.game.tournament.team.LineupStatus"%>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="tournament.team.showLineup.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPane">
			<div class="selectedTournament">
				<g:link controller="tournament" action="show"
					params="[teamId: session.team.id]">
					<g:message code="tournament.home" />
				</g:link>
			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>

		<h1>
			<g:message code="tournament.team.showLineup.title" />
		</h1>
		<h2>
			<g:message code="tournament.team.showLineup.subtitle"
				args="${subtitleArgsView}" />
		</h2>



		<div id="notificationsFlashPane">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPane" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<g:if test="${flash.KOMessage}">
				<div id="notificationKOPane" style="background-color: red;">
					${flash.KOMessage}
				</div>
			</g:if>

			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPane" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>

		<p name="LineupStatus">
			<g:if test="${lineupStatusView==LineupStatus.SAVED_OK}">
				<div style="background-color: lightgreen;">
					<g:message code="tournament.team.showLineup.status.saved.ok"
						args="${lineupFormationView}" />
				</div>
			</g:if>
			<g:elseif test="${lineupStatusView==LineupStatus.SAVED_INVALID}">
				<div style="background-color: red;">
					<g:message code="tournament.team.showLineup.status.saved.invalid"
						args="${lineupFormationView}" />
				</div>

			</g:elseif>
			<g:elseif test="${lineupStatusView==LineupStatus.NOT_SAVED}">
				<div style="background-color: yellow;">
					<g:message code="tournament.team.showLineup.status.not.saved" />
				</div>
			</g:elseif>
		</p>

		<g:form name="lineupForm" action="saveLineup" controller="team">
			<div id="lineupPanel">
				<g:hiddenField name="realTournament"
					value="${lineupRealTournamentView}" />
				<g:hiddenField name="matchWeek" value="${lineupMatchWeekView}" />
				<div class="GoalkeepersPanel">
					<h2>
						<g:message code="tournament.team.showLineup.div.goalkeepers" />
					</h2>

					<g:each var="goalkeeper" in="${goalkeepersView}">
						<li>(Name = ${goalkeeper.realFootballer.name}, Position = ${goalkeeper.currentPosition},
							RealTeam = ${goalkeeper.realFootballer.realTeam.name}, Status = ${goalkeeper.lineupFootballerStatus})
							<g:radioGroup name="${goalkeeper.realFootballer.id}"
								labels="${rgLabelsView}" values="${rgValuesView}"
								value="${goalkeeper.lineupFootballerStatus}">
								${it.label}
								${it.radio},
							</g:radioGroup>
						</li>
						<g:hiddenField name="footballerIds"
							value="${goalkeeper.realFootballer.id}" />

					</g:each>
				</div>

				<div class="DefendersPanel">
					<h2>
						<g:message code="tournament.team.showLineup.div.defenders" />
					</h2>
					<g:each var="defender" in="${defendersView}">
						<li>(Name = ${defender.realFootballer.name}, Position = ${defender.currentPosition},
							RealTeam = ${defender.realFootballer.realTeam.name}, Status = ${defender.lineupFootballerStatus})
							<g:radioGroup name="${defender.realFootballer.id}"
								labels="${rgLabelsView}" values="${rgValuesView}"
								value="${defender.lineupFootballerStatus}">
								${it.label}
								${it.radio},
							</g:radioGroup>
						</li>
						<g:hiddenField name="footballerIds"
							value="${defender.realFootballer.id}" />
					</g:each>
				</div>

				<div class="MidfieldersPanel">
					<h2>
						<g:message code="tournament.team.showLineup.div.midfielders" />
					</h2>

					<g:each var="midfielder" in="${midfieldersView}">
						<li>(Name = ${midfielder.realFootballer.name}, Position = ${midfielder.currentPosition},
							RealTeam = ${midfielder.realFootballer.realTeam.name}, Status = ${midfielder.lineupFootballerStatus})
							<g:radioGroup name="${midfielder.realFootballer.id}"
								labels="${rgLabelsView}" values="${rgValuesView}"
								value="${midfielder.lineupFootballerStatus}">
								${it.label}
								${it.radio},
							</g:radioGroup>
						</li>
						<g:hiddenField name="footballerIds"
							value="${midfielder.realFootballer.id}" />

					</g:each>
				</div>

				<div class="StrikersPanel">
					<h2>
						<g:message code="tournament.team.showLineup.div.strikers" />
					</h2>

					<g:each var="striker" in="${strikersView}">
						<li>(Name = ${striker.realFootballer.name}, Position = ${striker.currentPosition},
							RealTeam = ${striker.realFootballer.realTeam.name}, Status = ${striker.lineupFootballerStatus})

							<g:radioGroup name="${striker.realFootballer.id}"
								labels="${rgLabelsView}" values="${rgValuesView}"
								value="${striker.lineupFootballerStatus}">
								${it.label}
								${it.radio},
							</g:radioGroup>

						</li>
						<g:hiddenField name="footballerIds"
							value="${striker.realFootballer.id}" />
					</g:each>
				</div>


			</div>
			<div class="lineupForm">
				<g:submitButton name="Save" value="Save lineup" />
			</div>
		</g:form>
	</g:if>

</body>
</html>