<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="layout" content="main">
<title><g:message code="tournament.create.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPane">
			<div class="tournamentsPane">
				<!--<g:message code="tournament.list.options.list" /> -->
				<g:link controller="tournament" action="listByPlayer">
					<g:message code="tournament.list.options.list" />
				</g:link>

			</div>
			<div class="tournamentsPane">
				<g:message code="tournament.list.options.create" />
			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>
		</div>

		<h1>
			<g:message code="tournament.create.title" />

		</h1>

		<div id="notificationsFlashPane">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPane" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<g:if test="${flash.KOMessage}">
				<div id="notificationKOPane" style="background-color: red;">
					${flash.KOMessage}
				</div>
			</g:if>

			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPane" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>

		<!-- Errors -->
		<g:hasErrors>
			<div id="createTournamentErrorsPane" style="background-color: red;">
				<g:renderErrors bean="${errors}" as="list" />
			</div>
		</g:hasErrors>
		

		<div id="createTournamentPane">
			<g:form name="createTournamentForm" controller="tournament" action="create">
				<div class="formField">
					<label for="name"><g:message code="tournament.create.form.label.name" />
					</label>
					<g:textField name="name" value="${tournament?.name}" />
				</div>
				<div class="formField">
					<label for="description"><g:message code="tournament.create.form.label.description" />
					</label>
					<g:textField name="description" value="${tournament?.description}" />
				</div>				
				<div class="formField">
					<g:submitButton name="createButton" value="Create" />
				</div>
			</g:form>
		</div>
	</g:if>

</body>
</html>