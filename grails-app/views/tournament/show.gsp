<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="tournament.show.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPanel">
			<div class="tournamentsPanel">
				<g:link controller="tournament" action="listByPlayer">
					<g:message code="tournament.list.options.list" />
				</g:link>

			</div>
			<div class="tournamentsPanel">
				<g:link controller="tournament" action="create">
					<g:message code="tournament.list.options.create" />
				</g:link>

			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>

		<h1>
			<g:message code="tournament.show.title"
				args="${[team.tournament.name]}" />

		</h1>

		<div id="notificationsFlashPanel">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPanel" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<g:if test="${flash.KOMessage}">
				<div id="notificationKOPanel" style="background-color: red;">
					${flash.KOMessage}
				</div>
			</g:if>

			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPanel" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>


		<div id="tournamentShowPanel">
			<div id="myTeamPanel">
			<g:message code="tournament.show.team.description"
				args="${[team.player.login,team.name,team.teamRole,team.money, team.balance]}" />						
			</div>


			<div id="tournamentSettingsPanel">
				<g:if test="${isAdmin}">
					<div class="tournamentSettingsPanel">
						<g:link controller="tournament" action="showSettings">
							<g:message code="tournament.option.edit.settings" />
						</g:link>
					</div>
				</g:if>
				<g:else>
					<g:if test="${isMember}">
						<div class="tournamentSettingsPanel">
							<g:link controller="tournament" action="showSettings">
								<g:message code="tournament.option.show.settings" />
							</g:link>
						</div>
					</g:if>
				</g:else>
			</div>
			<div class="listFootballersPanel">
				<g:link controller="tournament" action="listFootballers">
					<g:message code="tournament.options.listFootballers" />
				</g:link>

			</div>
			<div class=listTeamFootballers>
				<g:link controller="tournament" action="listTeamFootballers">
					<g:message code="tournament.options.listTeamFootballers" />
				</g:link>

			</div>			
			<div class="lineup">
				<g:link controller="team" action="showLineup">
					<g:message code="tournament.options.team.showLineup" />
				</g:link>

			</div>			

			<div id="teamsPanel">
				<h2>
					<g:message code="tournament.show.teams" />
				</h2>
				<g:each var="nextTeam" in="${team.tournament.teams}">
					<ul>
						<li>Player:${nextTeam.player.login} Team: ${nextTeam.name}
							TeamRole: ${nextTeam.teamRole}
						</li>
					</ul>
				</g:each>
			</div>


		</div>

	</g:if>
</body>
</html>