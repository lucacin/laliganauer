<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="tournament.settings.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPane">
			<div class="tournamentsPane">
				<g:link controller="tournament" action="listByPlayer">
					<g:message code="tournament.list.options.list" />
				</g:link>

			</div>
			<div class="tournamentsPane">
				<g:link controller="tournament" action="create">
					<g:message code="tournament.list.options.create" />
				</g:link>

			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>

		<h1>
			<g:message code="tournament.settings.title"
				args="${[session.team.tournament.name]}" />

		</h1>


		<div id="notificationsFlashPane">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPane" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<div id="errorsPane">
				<g:if test="${flash.KOMessage}">
					<div id="notificationKOPane" style="background-color: red;">
						${flash.KOMessage}
					</div>
				</g:if>
				<g:hasErrors>
					<div style="background-color: red;">
						<g:renderErrors bean="${tournament}" as="list" />
					</div>
				</g:hasErrors>
			</div>


			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPane" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>


		<div id="tournamentSettingsPane">
			<div class="deletePane">
				<g:if test="${isAdmin}">
					<g:link controller="tournament" action="delete">
						<g:message code="tournament.option.delete" />
					</g:link>
				</g:if>
			</div>

			<g:form controller="tournament" action="update" name="editForm">

				<div class="formField">
					<label for="name"><g:message
							code="settings.tournament.form.label.name" /></label>
					<g:textField name="name" readonly="${!isAdmin}"
						value="${tournament?.name}" />
				</div>

				<div class="formField">
					<label for="description"><g:message
							code="settings.tournament.form.label.description" /></label>
					<g:textField name="description" readonly="${!isAdmin}"
						value="${tournament?.description}" />
				</div>


				<div class="formField">

					<g:if test="${tournament.isInAEditableStatus()}">
						<label for="status"><g:message
								code="settings.tournament.form.label.status" /></label>

						<g:if test="${isAdmin}">
							<g:select name="status"
								from="${tournament.getEditableStatuses()}"
								valueMessagePrefix="settings.tournament.form.values.status"
								value="${tournament?.status}" />
						</g:if>
						<g:else>
							<g:select name="status" from="${tournament?.status}"
								valueMessagePrefix="settings.tournament.form.values.status"
								value="${tournament?.status}" />
						</g:else>
					</g:if>
					<g:else>
						<label for="status"><g:message
								code="settings.tournament.form.label.status" /></label>

						<g:select name="status"
							from="${tournament.getNoEditableStatuses()}" readonly="true"
							valueMessagePrefix="settings.tournament.form.values.status"
							value="${tournament?.status}" />
					</g:else>
				</div>

				<div class="formField">
					<label for="reference"><g:message
							code="settings.tournament.form.label.reference" /></label>
					<g:textField name="reference" readonly="true"
						value="${tournament?.reference}" />
				</div>


				<div class="formField">
					<label for="numberOfTeams"><g:message
							code="settings.tournament.form.label.numberOfTeams" /></label>
					<g:field name="numberOfTeams" type="number" readonly="true"
						value="${tournament?.numberOfTeams}" />
				</div>


				<div class="formField">
					<label for="tournamentSettings.max_teams"><g:message
							code="settings.tournament.form.label.max_teams" /></label>
					<g:field name="tournamentSettings.max_teams" type="number"
						readonly="true"
						value="${tournament?.tournamentSettings?.max_teams}" />
				</div>
				<g:if test="${isAdmin}">
					<g:submitButton class="formButton" name="update" value="update" />
				</g:if>
			</g:form>

		</div>

	</g:if>
</body>
</html>
