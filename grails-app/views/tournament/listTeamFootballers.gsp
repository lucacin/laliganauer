<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="tournament.listTeamFootballers.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPane">
			<div class="selectedTournament">
				<g:link controller="tournament" action="show"
					params="[teamId: session.team.id]">
					<g:message code="tournament.home" />
				</g:link>
			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>

		<h1>
			<g:message code="tournament.listTeamFootballers.title" />

		</h1>

		<div id="notificationsFlashPane">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPane" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<g:if test="${flash.KOMessage}">
				<div id="notificationKOPane" style="background-color: red;">
					${flash.KOMessage}
				</div>
			</g:if>

			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPane" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>

        <div id="listTeamFootballersPanel">
            <g:each var="nextFootballer" in="${footballersView}">
                <g:form name="saleFootballerForm" controller="team"
                    action="sellFootballer">
                    <div class="formField">
                        <li>(Name = ${nextFootballer.realFootballer.name}, Position = ${nextFootballer.realFootballer.position},
                            RealTeam = ${nextFootballer.realFootballer.realTeam.name}, Price =
                            ${nextFootballer.realFootballer.price})
                        </li>
                        <g:hiddenField name="footballerId" value="${nextFootballer.id}" />
                        <g:submitButton name="saleFootballer" value="${sellButtonLabelView}" />
                    </div>
                </g:form>
            </g:each>
        </div>
	</g:if>

</body>
</html>