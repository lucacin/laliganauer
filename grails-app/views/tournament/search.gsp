<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="tournament.seach.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPane">
			<div class="tournamentsPane">
				<g:link controller="tournament" action="listByPlayer">
					<g:message code="tournament.list.options.list" />
				</g:link>

			</div>
			<div class="tournamentsPane">
				<g:link controller="tournament" action="create">
					<g:message code="tournament.list.options.create" />
				</g:link>

			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>

		<h1>
			<g:message code="tournament.search.title" />

		</h1>

		<div id="notificationsFlashPane">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPane" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<g:if test="${flash.KOMessage}">
				<div id="notificationKOPane" style="background-color: red;">
					${flash.KOMessage}
				</div>
			</g:if>

			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPane" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>

		<div id="tournamentSearch">
			<g:form name="tournamentSearchForm" controller="tournament"
				action="search">
				<div class="formField">
					<g:textField name="searchText" value="" />
					<g:submitButton name="searchButton" value="Search" />
				</div>
			</g:form>

		</div>


		<div id="tournamentSearchResultByReferencePane">
			<h2>
				<g:message code="tournament.search.results.reference.title" />
			</h2>
			<g:if test="${tournamentByReference == null}">
				<g:message code="tournament.search.noTournaments.ByReference" />
			</g:if>
			<g:else>
				<p>
					Tournament [${tournamentByReference.reference}]:
					${tournamentByReference.name}
				</p>
			</g:else>
		</div>

		<div id="tournamentSearchResultByNamePane">
			<h2>
				<g:message code="tournament.search.results.name.title" />
			</h2>

			<g:if test="${tournamentsByName.size() == 0}">
				<g:message code="tournament.search.noTournaments.ByName" />
			</g:if>
			<g:else>

				<g:each var="nextTournament" in="${tournamentsByName}">
					<g:if test="${nextTournament.isInAJoinableStatus()}">
						<li>Reference [${nextTournament.reference}]: Name: ${nextTournament.name}
							Status: ${nextTournament.status} <g:link controller="tournament"
								action="join" params="[tournamentId: nextTournament.id]">
								<g:message code="tournament.join.link.title" />
							</g:link>
						</li>
					</g:if>
					<g:else>
						<li>Reference [${nextTournament.reference}]: Name: ${nextTournament.name}
							Status: ${nextTournament.status}
						</li>

					</g:else>
				</g:each>

				<g:paginate controller="tournament" action="search"
					max="${maxResultsPerPage}" total="${tournamentsByNameCount}"
					params="[searchText:searchText]" />

			</g:else>

		</div>

	</g:if>
</body>
</html>