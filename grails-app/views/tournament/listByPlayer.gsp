<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="layout" content="main" />
<title><g:message code="tournament.list.tab.title" /></title>
</head>
<body id="body">
	<g:if test="${session.authenticated}">

		<div id="optionsPane">
			<div class="tournamentsPane">
				<g:link controller="tournament" action="listByPlayer">
					<g:message code="tournament.list.options.list" />
				</g:link>

			</div>
			<div class="tournamentsPane">
				<g:link controller="tournament" action="create">
					<g:message code="tournament.list.options.create" />
				</g:link>

			</div>
			<div class="logout">
				<g:link controller="player" action="logout">
					<g:message code="player.home.options.logout" />
				</g:link>
			</div>

		</div>

		<h1>
			<g:message code="tournament.listByPlayer.title"
				args="${[session.player.login]}" />

		</h1>

		<div id="notificationsFlashPane">
			<g:if test="${flash.OKMessage}">
				<div id="notificationOKPane" style="background-color: lightgreen;">
					${flash.OKMessage}
				</div>
			</g:if>

			<g:if test="${flash.KOMessage}">
				<div id="notificationKOPane" style="background-color: red;">
					${flash.KOMessage}
				</div>
			</g:if>

			<g:if test="${flash.infoMessage}">
				<div id="notificationInfoPane" style="background-color: blue;">
					${flash.infoMessage}
				</div>
			</g:if>
		</div>

		<div id="tournamentSearch">
			<g:form name="tournamentSearchForm" controller="tournament"
				action="search">
				<div class="formField">
					<g:textField name="searchText" value="" />
					<g:submitButton name="searchButton" value="Search" />
				</div>
			</g:form>

		</div>


		<div id="tournamentListByPlayerPane">
			<g:if test="${teamsView.size() == 0}">
				<g:message code="tournament.listByPlayer.noTournaments" />
			</g:if>
			<g:else>
				<g:each var="nextTeam" in="${teamsView}">
					<li>Tournament [${nextTeam.tournament.reference}]: Team: ${nextTeam.name} Tournament: <g:link
							controller="tournament" action="show"
							params="[teamId: nextTeam.id]">
							${nextTeam.tournament.name}
						</g:link> TeamRole: ${nextTeam.teamRole} <g:link controller="tournament"
							action="leave" params="[teamId: nextTeam.id]">
							<g:message code="tournament.leave.link.title" />
						</g:link>
					</li>
				</g:each>

			</g:else>
		</div>
	</g:if>
</body>
</html>