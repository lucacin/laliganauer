class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		//"/"(controller:"player")
		"/"(view:"player/login")
		"500"(view:'/error')
	}
}
