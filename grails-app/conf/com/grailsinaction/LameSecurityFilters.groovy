package com.grailsinaction

import com.laliganauer.game.player.PlayerController
import com.laliganauer.game.tournament.team.Team
import com.laliganauer.game.tournament.team.TeamRole
import com.laliganauer.game.tournament.Tournament
import com.laliganauer.game.player.Player

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsHttpSession

class LameSecurityFilters {

	// Session parameter is needed
	public void setAuthentication (HttpSession session,Player player) {
		session.player = player
		session.authenticated=true
	}

	// Session parameter is needed
	private boolean isAuthenticatedAccess(HttpSession session) {
		return session.player != null && session.authenticated ==true
	}

	public boolean isTournamentMember (Player lamePlayer,Team lameTeam) {
		if ((lamePlayer!=null) && (lameTeam!=null) ) {
			return lamePlayer.id == lameTeam.player.id
		}
		return false
	}

	public boolean isTournamentAdminMember (Player lamePlayer,Team lameTeam) {
		boolean member = this.isTournamentMember(lamePlayer,lameTeam)
		if (member) {
			return lameTeam.teamRole == TeamRole.ADMIN
		}
		return false
	}

	private void logErrorFilter (String controllerNameParam,String actionNameParam,  HttpServletRequest requestParam) {
		def headers = [:]
		requestParam.getHeaderNames().each { header ->
			headers["$header"] = requestParam.getHeader("$header")
		}
		log.warn "Not authorized action [controller=${controllerNameParam}, action=${actionNameParam}, queryString=${requestParam.getQueryString()}, headers=${headers}]"
	}
	
	private void notAuthenticatedErrorActions (String controllerNameParam,String actionNameParam,  HttpServletRequest requestParam) {
		def playerController = new PlayerController()
		playerController.flash.KOMessage = playerController.message(code: "error.notauthenticated.player")
		this.logErrorFilter(controllerNameParam,actionNameParam,requestParam)
		playerController.redirect (uri: '/')
	}

	private void notMemberErrorActions (String controllerNameParam,String actionNameParam,  HttpServletRequest requestParam) {
		def playerController = new PlayerController()
		playerController.flash.KOMessage = playerController.message(code: "error.notmember.player")
		this.logErrorFilter(actionNameParam,controllerNameParam,requestParam)
		playerController.redirect (uri: '/tournament/listByPlayer')
	}
	
	private void notAdminMemberErrorActions (String controllerNameParam,String actionNameParam,  HttpServletRequest requestParam) {
		def playerController = new PlayerController()
		playerController.flash.KOMessage = playerController.message(code: "error.notadmin.player")
		this.logErrorFilter(actionNameParam,controllerNameParam,requestParam)
		playerController.redirect (uri: '/tournament/listByPlayer')
	}


	def filters = {
		//FIXME: Include / to filters, if is player redirect to home
		notAuthenticatedPlayerActions(controller:'player', action:'(login|register|forgotPassword)') {
			before = {
				if (this.isAuthenticatedAccess(session)) {
					redirect (uri: '/player/home')
					return false
				}
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}

		authenticatedPlayerActions(controller:'player', action:'(home|logout)') {
			before = {
				if (!this.isAuthenticatedAccess(session)) {
					this.notAuthenticatedErrorActions(controllerName,actionName,request)															
					return false
				}
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}

		authenticatedTournamentActions(controller:'tournament', action:'(create|listByPlayer|search|join)') {
			before = {
				if (!this.isAuthenticatedAccess(session)) {
					this.notAuthenticatedErrorActions(controllerName,actionName,request)
					return false
				}
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}

		}

		memberTournamentActions(controller:'tournament', action:'(leave|show|showSettings|listFootballers)') {
			before = {
				boolean result = false
				def teamFound = null
				log.debug "los params de memberTournament son $params"

				if (params.teamId != null) {
					def teamId=params.teamId
					teamFound = Team.get(teamId)
				}
				else {
					if (session.team.id!=null) {
						teamFound=session.team
					}
				}

				def player = session.player
				

				if (this.isTournamentMember(player, teamFound)) {
					result = true
				}
				else
				{
					this.notMemberErrorActions(controllerName,actionName,request)
				}

				return result
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}
		
		memberTournamentActions2(controller:'team', action:'(showLineup)') {
			before = {
				boolean result = false
				def teamFound = null
				log.debug "los params de memberTournament son $params"

				if (params.teamId != null) {
					def teamId=params.teamId
					teamFound = Team.get(teamId)
				}
				else {
					if (session.team.id!=null) {
						teamFound=session.team
					}
				}

				def player = session.player
				

				if (this.isTournamentMember(player, teamFound)) {
					result = true
				}
				else
				{
					this.notMemberErrorActions(controllerName,actionName,request)
				}

				return result
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}

		adminTournamentActions(controller:'tournament', action:'(update|delete)') {
			before = {
				boolean result = false
				if (this.isTournamentAdminMember(session.player, session.team)) {
					result = true
				}
				else {
					this.notAdminMemberErrorActions(controllerName,actionName,request)
				}
				return result
			}
			after = { Map model ->
			}
			afterView = { Exception e ->
			}
		}
	}


}

