import grails.util.Environment

import com.laliganauer.game.GlobalSettingsService;
import com.laliganauer.real.RealFootballer
import com.laliganauer.real.RealTournament
import com.laliganauer.real.RealTeam
import com.laliganauer.real.RealMatchWeek

class BootStrap {
	String realTournamentCSVPath = "./web-app/datafiles/csv/real_tournaments.csv"
	String realMatchWeeksCSVPath = "./web-app/datafiles/csv/real_matchweeks.csv"
	String realTeamCSVPath = "./web-app/datafiles/csv/real_teams.csv"
	String footballerCSVPath = "./web-app/datafiles/csv/real_footballers.csv"

	def loadData() {
		if (RealTournament.count()==0) {
			log.info 'Loading real tournaments ...'
			def realTournamentCSV = new File(realTournamentCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading Tournament (name: "${fields[0]}", startDate: "${fields[1]}", endDate: "${fields[2]})"""
				def newRealTournament = new RealTournament(name: fields[0].trim(), startDate: Date.parse ("dd/MM/yyyy",fields[1].trim()),endDate:Date.parse ("dd/MM/yyyy",fields[2].trim()))
				newRealTournament.save(failOnError: true, flush: true)
			}
		}

		if (RealMatchWeek.count()==0) {
			log.info 'Loading real matchweeks ...'
			def realMatchWeeksCSV = new File(realMatchWeeksCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading Matchweek (number: "${fields[0]}", startDate: "${fields[1]}",endDate: "${fields[2]}", FK: "${fields[3]}")"""
				def newRealMatchWeek = new RealMatchWeek(number: fields[0],startDate: Date.parse ("dd/MM/yyyy",fields[1].trim()),endDate: Date.parse ("dd/MM/yyyy",fields[2].trim()))				
				def foundRealTournament = RealTournament.where { name == "${fields[3]}"}.get()
				foundRealTournament.addToRealMatchWeeks(newRealMatchWeek)
				newRealMatchWeek.save(failOnError: true, flush: true)
			}
		}

		if (RealTeam.count() == 0) {
			log.info 'Loading real teams ...'
			def realTeamCSV = new File(realTeamCSVPath).splitEachLine(",") { fields ->
				log.debug """Loading RealTeam (name: "${fields[0]}", stadiumName: "${fields[1]}", FK: "${fields[2]}")"""
				def newRealTeam = new RealTeam(name: fields[0].trim(), stadiumName: fields[1].trim())
				newRealTeam.save(failOnError: true, flush: true)
				def foundRealTournament = RealTournament.where { name == "${fields[2]}"}.get()
				foundRealTournament.addToRealTeams(newRealTeam)
			}
		}
		
		if (RealFootballer.count() == 0) {
		 log.info 'Loading footballers ...'
		 def footballerCSV = new File(footballerCSVPath).splitEachLine(",") { fields ->
			 log.debug """Loading Footballer (name: "${fields[0]}", position: "${fields[1]}", FK: "${fields[2]}, price: "${fields[3]}"")"""
			 def newRealFootballer = new RealFootballer(name: fields[0].trim(), position: fields[1].trim(),price : Integer.parseInt(fields[3]))
			 newRealFootballer.save(failOnError: true, flush: true)
			 def foundRealTeam = RealTeam.where { name == "${fields[2]}"}.get()
			 foundRealTeam.addToRealFootballers(newRealFootballer)
		 }
		}
		
	}

	def createGlobalSettings () {
		def globalSettings = new GlobalSettingsService()
		globalSettings.createTournamentMaxTeamsDefault(15)
		globalSettings.createCurrentRealTournament('Liga 2012-2013')
		globalSettings.createCurrentRealMatchWeek(1)
		globalSettings.createTeamInitialAmount(100000000)
	}
	
	def init = { servletContext ->
		log.info "Bootstrap in environment type/name: ${Environment.current} / ${Environment.current.name}"		
		environments {
			development_postgresql { 
				this.loadData() 
				this.createGlobalSettings()
			}
			development {this.loadData()}
			mysql {this.loadData()}
		}
	}

	def destroy = {
	}
}
