package com.laliganauer.game.player

import com.laliganauer.game.player.ForgotPasswordCommand
import com.laliganauer.game.player.LoginCommand
import com.laliganauer.game.player.RegisterCommand
import com.laliganauer.game.player.Player

import grails.validation.ValidationException

class PlayerException extends RuntimeException {
	String message
	Player player
}


class PlayerService {
	// Grails automatically sets service transactional
	static transactional = true

	def Player login (LoginCommand loginCommand) throws ValidationException  {

		if (loginCommand.validate()) {
			def foundPlayer = Player.findByLogin(loginCommand.login)
			if (foundPlayer) {
				if (foundPlayer.checkPassword(loginCommand.password)) {
					return foundPlayer
				}
				else {
					loginCommand.errors.rejectValue("password","com.laliganauer.game.player.LoginCommand.password.incorrect",["${loginCommand.login}"] as Object[],"Default message")
				}
			}
			else {
				loginCommand.errors.rejectValue("login","com.laliganauer.game.player.LoginCommand.login.not.found",["${loginCommand.login}"] as Object[],"Default message")
			}
		}

		throw new ValidationException(loginCommand.errors.getAllErrors().toString(),loginCommand.errors)
	}

	def Player register (RegisterCommand registerCommand) throws ValidationException {
		def player = new Player(registerCommand.properties)

		// validate registerCommand
		if (registerCommand.validate()) {
			player.setPasswordToHash(registerCommand.password)
			// Automatically validate all player fields and throw ValidationException if there is an error
			if (player.save(validation: true, failOnError:true)) {
				return player
			}
		}
		else {
			throw new ValidationException(registerCommand.errors.getAllErrors().toString(),registerCommand.errors)
		}
	}

	def Player forgotPasswordByLogin (ForgotPasswordCommand forgotPasswordCommand) throws ValidationException {
		def player = new Player(forgotPasswordCommand.properties)

		if (forgotPasswordCommand.validate([
			'login',
			'password',
			'confirmPassword'
		])) {
			def foundPlayer = Player.findByLogin(player.login)
			if (foundPlayer) {
				//TODO: sent confirmation link by mail
				foundPlayer.setPasswordToHash(forgotPasswordCommand.password)
				if (foundPlayer.save (validation: true, failOnError:true)){
					return foundPlayer
				}
			}
			else {
				forgotPasswordCommand.errors.rejectValue("login","com.laliganauer.game.player.ForgotPasswordCommand.login.not.found",[
					"${forgotPasswordCommand.login}"] as Object[],"Default message")
			}
		}
		throw new ValidationException(forgotPasswordCommand.errors.getAllErrors().toString(),forgotPasswordCommand.errors)
	}

	def Player forgotPasswordByEmail (ForgotPasswordCommand forgotPasswordCommand) throws ValidationException {

		def player = new Player(forgotPasswordCommand.properties)

		if (forgotPasswordCommand.validate([
			"email",
			"password",
			"confirmPassword"
		])) {
			def foundPlayer = Player.findByEmail(player.email)
			if (foundPlayer) {
				//TODO: sent confirmation link by mail
				foundPlayer.setPasswordToHash(forgotPasswordCommand.password)
				if (foundPlayer.save(validation: true, failOnError:true)){
					return foundPlayer
				}
			}
			else {
				forgotPasswordCommand.errors.rejectValue("email","com.laliganauer.game.player.ForgotPasswordCommand.email.not.found",[
					"${forgotPasswordCommand.email}"] as Object[],"Default message")
			}
		}
		throw new ValidationException(forgotPasswordCommand.errors.getAllErrors().toString(),forgotPasswordCommand.errors)
	}

	def serviceMethod() {
	}
}
