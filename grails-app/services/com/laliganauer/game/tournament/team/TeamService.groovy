package com.laliganauer.game.tournament.team

import java.util.List

import com.grailsinaction.LameSecurityFilters
import com.laliganauer.game.GlobalSettingsService
import com.laliganauer.game.tournament.Footballer
import com.laliganauer.game.tournament.team.Team
import com.laliganauer.game.player.Player
import com.laliganauer.real.Position
import com.laliganauer.real.RealFootballer
import com.laliganauer.real.RealMatchWeek
import com.laliganauer.game.tournament.team.LineupStatus
import com.laliganauer.game.tournament.team.Lineup



class TeamService {

	def List <Team> listByPlayer(Player playerSession) {
		log.debug "Service list ByPlayer $playerSession"
		def results = Team.where {
			player.id == playerSession.id
		}.list()
		return results
	}

	def Map showLineup (Player player,Team param_team) {
		//get all Team footballers
		def tournament = param_team.tournament
		if (tournament!=null) {
			def securityFilters = new LameSecurityFilters()
			boolean isMember = securityFilters.isTournamentMember(player, param_team)
			if (isMember) {
				//get all team footballers
				def teamFootballersCriteria = Footballer.createCriteria()
				def allTeamFootballers = teamFootballersCriteria.list () {
					team { idEq (param_team.id) }
				}

				//get lineup footballers
				def globalSettings = new GlobalSettingsService()
				def currentRealTournamentName = globalSettings.getCurrentRealTournament()
				def currentRealMatchWeek = globalSettings.getCurrentRealMatchWeek()

				def lineupCriteria = Lineup.createCriteria()
				def currentLineupResult = lineupCriteria.get {
					and {
						team { idEq(param_team.id) }
						realMatchWeek {
							and {
								realTournament { eq("name",currentRealTournamentName) }
								eq("number",currentRealMatchWeek)
							}
						}
					}
				}

				def result =[:]
				result['currentRealMatchWeek']=currentRealMatchWeek
				result['currentRealTournamentName']=currentRealTournamentName
				result['resultFootballersList']=[]
				//def resultFootballersList = []
				//if there is no lineup, all footballers are NOT_SELECTED
				if (currentLineupResult==null) {
					for (iterTeam in allTeamFootballers) {
						result['resultFootballersList'].add(new LineupFootballer (realFootballer:iterTeam.realFootballer, lineupFootballerStatus: LineupFootballerStatus.NOT_SELECTED,currentPosition:iterTeam.realFootballer.position,points:0))
					}
					result['lineupStatus'] = LineupStatus.NOT_SAVED
					return result
				}
				else  {
					//create a unique list with all footballers
					result['resultFootballersList']= currentLineupResult.lineupFootballers


					for (iterTeam in allTeamFootballers) {
						def inList = false
						for (iterResult in result['resultFootballersList']){
							if (iterTeam.realFootballer.id == iterResult.realFootballer.id) {
								inList = true
								break
							}
						}
						if (!inList) {
							result['resultFootballersList'].add(new LineupFootballer (realFootballer:iterTeam.realFootballer, lineupFootballerStatus: LineupFootballerStatus.NOT_SELECTED,currentPosition:iterTeam.realFootballer.position,points:0))
						}

					}
					result['lineupStatus'] = currentLineupResult.lineupStatus
					result['formation']= currentLineupResult.currentFormation
					return 	result
				}

			}
		}
	}



	def Lineup saveLineup (Team team_param,String tournament_param, Integer matchweek_param, List <SaveLineupCommand> lineupFootballers_param) {

		Lineup lineup = new Lineup(points:0)

		//get lineup footballers
		def globalSettings = new GlobalSettingsService()
		def currentRealTournamentName = globalSettings.getCurrentRealTournament()
		def currentRealMatchWeek = globalSettings.getCurrentRealMatchWeek()

		//Check if tournament and matchweek are the correct ones
		if ((tournament_param==currentRealTournamentName) && (matchweek_param==currentRealMatchWeek)) {

			//Check that footballers are assigned to the correct team
			for (lf in lineupFootballers_param) {
				def criteria = Footballer.createCriteria()

				def footballerFound = criteria.get{
					and {
						realFootballer { idEq(lf.realFootballerId) }
						tournament { idEq(team_param.tournament.id) }
					}
				}
				if (footballerFound!=null) {
					if ((footballerFound.team !=null) &&(footballerFound.team.id==team_param.id)) {
						def lineupFootballer = new LineupFootballer(realFootballer:footballerFound.realFootballer,lineupFootballerStatus:lf.status,currentPosition:footballerFound.realFootballer.position,points: 0)
						lineup.addLineupFootballer(lineupFootballer)
					}
					else {
						//TODO:	throw FootballerNotBelongsToTeam
					}
				}
				else {
					//TODO:	throw FootballerNotFoundException
				}
			}

			//Validate formation
			lineup.validateAndUpdateFormation()

			//Create new lineup or edit
			def criteriaLineup = Lineup.createCriteria()
			def lineupFound = criteriaLineup.get{
				and {
					realMatchWeek { eq("number",matchweek_param)}
					team {idEq(team_param.id)}
				}
			}

			def teamFound = Team.get(team_param.id)

			if (teamFound!=null) {
				def criteriaMatchWeek = RealMatchWeek.createCriteria()
				def matchWeekFound = criteriaMatchWeek.get{
					and {
						eq("number",matchweek_param)
						realTournament { eq("name",tournament_param) }
					}
				}
				if (matchWeekFound!=null) {
					//delete old lineupFound
					if (lineupFound!=null) {
						teamFound.removeFromLineups(lineupFound)
						lineupFound.delete()
					}
					lineup.realMatchWeek = matchWeekFound
					teamFound.addToLineups(lineup)
					return lineup
				}
				else {
					//TODO: throw MatchWeekNotFoundException
				}
			}
			else {
				//TODO: throw TeamNotFoundException
			}

		}
		else {
			//TODO: throw IncorrectTournamentOrRealMatchWeekException
		}


	}

	def sellFootballer(Player player_param, Team team_param, Long footballerId) {
		//get lineup footballers
		def globalSettings = new GlobalSettingsService()
		def currentRealTournamentName = globalSettings.getCurrentRealTournament()
		def currentRealMatchWeek = globalSettings.getCurrentRealMatchWeek()

		//get Footballer
		def footballerFound = Footballer.get(footballerId)
		if (footballerFound!= null) {
			//check that the footballer is assigned to the correct team
			if ((footballerFound.team !=null) &&(footballerFound.team.id==team_param.id)) {

				
				def criteriaMatchWeek = RealMatchWeek.createCriteria()
				def matchWeekFound = criteriaMatchWeek.get{
					and {
						eq("number",currentRealMatchWeek)
						realTournament { eq("name",currentRealTournamentName) }
					}
				}

				//Check if the footballer is assigned to the current lineup
				def criteriaLineup = Lineup.createCriteria()
				def lineupFound = criteriaLineup.get{
					and {
						realMatchWeek { idEq(matchWeekFound.id)}
						team {idEq(team_param.id)}
					}
				}
				
				def footballerInLineup = false
	
				if (lineupFound != null) {
					for (lf in lineupFound.lineupFootballers) {
						if (lf.realFootballer.id == footballerFound.realFootballer.id) {
							lineupFound.removeFromLineupFootballers(lf)
							lf.delete()
							footballerInLineup = true
							break
						} 
					}
					//Validate formation
					lineupFound.validateAndUpdateFormation()
		
				}
				
				//add money
				def teamFound = Team.get(team_param.id)
				teamFound.balance=teamFound.balance + footballerFound.realFootballer.price
				teamFound.money=teamFound.money+footballerFound.realFootballer.price
				teamFound.save(failOnError: true)
				
				//free the footballer
				footballerFound.team = null
				footballerFound.save(failOnError: true)
				
				return [footballer: footballerFound, footballerInLineup: footballerInLineup]			
			}

		}
		else {
			//TODO: throw Exception FootballerNotFound
		}


	}
	def serviceMethod() {

	}
}
