package com.laliganauer.game.tournament

import com.grailsinaction.LameSecurityFilters
import com.laliganauer.game.GlobalSettingsService
import com.laliganauer.game.tournament.Tournament
import com.laliganauer.game.tournament.TournamentSettings
import com.laliganauer.game.tournament.TournamentStatus
import com.laliganauer.game.tournament.team.Team
import com.laliganauer.game.tournament.team.TeamRole
import com.laliganauer.game.tournament.CreateTournamentCommand
import com.laliganauer.game.player.Player
import com.laliganauer.real.RealTournament
import com.laliganauer.real.RealFootballer


import grails.validation.ValidationException
import org.hibernate.collection.PersistentSet



class CreateTournamentCommandException extends RuntimeException {
	String message
	CreateTournamentCommand createTournamentCommand
}


class TournamentException extends RuntimeException {
	String message
	Tournament tournament
}


class CreateTournamentException extends RuntimeException {
	String message
	CreateTournamentCommand createTournamentCommand
}

class CreateTournamentNotRealTournamentFoundException extends CreateTournamentException {
}



class JoinTournamentException extends RuntimeException {
	String message
	Tournament tournament
}

class JoinTournamentNotFoundException extends JoinTournamentException {
}
class JoinTournamentAlreadyRegisteredException extends JoinTournamentException {
}
class JoinTournamentClosedException extends JoinTournamentException {
}
class JoinTournamentFullException extends JoinTournamentException {
}

class LeaveTournamentException extends RuntimeException {
	String message
	Tournament tournament
}

class LeaveTournamentNotFoundException extends LeaveTournamentException {
}

class LeaveTournamentNotMemberException extends LeaveTournamentException {
}

class LeaveTournamentOnlyAdminException extends LeaveTournamentException {
}

class ShowTournamentException extends RuntimeException {
	String message
	Tournament tournament
}

class ShowTournamentNotFoundException extends ShowTournamentException {
}

class DeleteTournamentException extends RuntimeException {
	String message
	Tournament tournament
}

class DeleteTournamentNotAdminException extends DeleteTournamentException {
}

class DeleteTournamentGenericException extends DeleteTournamentException {
}

class TeamException extends RuntimeException {
	String message
	Team team
}

class ValidateTeamException extends RuntimeException {
	String message
	Team team
}

class UpdateTournamentException extends RuntimeException {
	String message
	Tournament tournament
}

class UpdateTournamentValidationException extends UpdateTournamentException {
}

class ListFootballersException extends RuntimeException {
	String message
	Tournament tournament
}

class ListFootballersNotFoundException extends ListFootballersException {
}

class ListFootballersNotMemberException extends ListFootballersException {
}

class ListFootballersGenericException extends ListFootballersException {
}



class TournamentService {

	// all methods are transactional
	static transactional = true

	/* Creates Tournament and associated admin team*/
	def Tournament createTournamentAndAssociatedTeam (CreateTournamentCommand createTournamentCommand, Player player, String teamName) throws ValidationException, CreateTournamentCommandException {


		if (createTournamentCommand.validate()) {
			def tournament = new Tournament(createTournamentCommand.properties)
			// set an unique ID (UUID)
			tournament.setRandomId()
			//set settings for this tournament
			def tournamentSettings = new TournamentSettings()
			def globalSettings = new GlobalSettingsService()
			tournament.status = TournamentStatus.OPENED
			tournament.numberOfTeams = 1
			tournamentSettings.max_teams= globalSettings.getTournamentMaxTeamsDefault()
			tournament.tournamentSettings=tournamentSettings

			//create footballers for the tournament based on realFootballers
			def currentRealTournamentName = globalSettings.getCurrentRealTournament()
			def realTournament =  RealTournament.findByName(currentRealTournamentName)
			log.debug "Creating tournament ${createTournamentCommand.name} based on realTournament ${currentRealTournamentName}"
			if (realTournament!=null) {
				for (realTeam in realTournament.realTeams) {
					for (realFootballer in realTeam.realFootballers) {
						log.debug "Footballer: ${realFootballer.name}, Team: ${realTeam.name}"
						tournament.addToFootballers(new Footballer(realFootballer: realFootballer, team: null))
					}
				}
			}
			else {
				//TODO: Standarize exceptions (inherit from CreateTournamentCommandException or CreateTournamentException)
				log.error "Throw exception"
			}

			if (tournament.save()) {
				def initialAmount = globalSettings.getTeamInitialAmount()
				def team = new Team(name: teamName, teamRole:TeamRole.ADMIN,player:player, tournament:tournament, money: initialAmount , balance:initialAmount)
				if (team.save()) {
					return tournament
				}
			}
		}
		else {
			throw new CreateTournamentCommandException (message:createTournamentCommand.errors.getAllErrors(), createTournamentCommand: createTournamentCommand)
		}
	}


	def Tournament searchByReference (String value) {
		// Look for UUID
		return Tournament.findByReference(value)
	}


	def List <Tournament> searchByNamePaginate (String value, int max, int offset) {
		// Look for Name
		def criteria = Tournament.createCriteria()
		def results = criteria.list (max: max, offset: offset) {
			ilike("name", "%$value%")
			order("name")
		}
		return results
	}


	def Team join (Player playerToJoin, int tournamentId, String teamName) throws
			JoinTournamentException,JoinTournamentAlreadyRegisteredException,JoinTournamentFullException,
			JoinTournamentClosedException,JoinTournamentNotFoundException,
	ValidateTeamException,TeamException {

		def tournamentFound = Tournament.get(tournamentId)
		if (tournamentFound!=null) {
			// Search if it is opened
			if (tournamentFound.isInAJoinableStatus()) {
				def maxTeams = tournamentFound.tournamentSettings.max_teams
				def currentNumberOfTeams = tournamentFound.numberOfTeams
				if ( currentNumberOfTeams < maxTeams) {
					// Check if it is already registered in this tournament
					def teamFound= Team.where {
						(player.id==playerToJoin.id
								&&
								tournament.id == tournamentId)
					}.find()

					if (teamFound==null) {
						def globalSettings = new GlobalSettingsService()
						def initialAmount = globalSettings.getTeamInitialAmount()
						def team = new Team(name: teamName, teamRole:TeamRole.PLAYER,player:playerToJoin, tournament:tournamentFound, money: initialAmount, balance: initialAmount)
						if (team.validate()) {
							if (team.save()) {
								tournamentFound.numberOfTeams = tournamentFound.numberOfTeams + 1
								if  (tournamentFound.numberOfTeams >= maxTeams) { // in theory never will be >
									tournamentFound.status = TournamentStatus.FULL
								}
								if (tournamentFound.save()) {
									return team
								}
								else {
									throw new TournamentException (message:"Tournament Saving Exception",tournament: tournamentFound)
								}
							}
							else {
								throw new TeamException (message:"Team Saving Exception",team: team)
							}
						}
						else {
							throw new ValidateTeamException (message:"Validate Team Exception", team: team)
						}
					}
					else {
						throw new JoinTournamentAlreadyRegisteredException (message:"Join: Already Registered Exception", tournament: tournamentFound)
					}
				}
				else {
					throw new JoinTournamentFullException (message:"Validate Team Exception", tournament: tournamentFound)
				}
			}
			else {
				throw new JoinTournamentClosedException (message:"Join: Closed Exception", tournament: tournamentFound)
			}
		}
		else {
			throw new JoinTournamentNotFoundException (message:"Join: Tournament Not Found Exception", tournament: tournamentFound)
		}
	}


	def Tournament leave (Player player, int teamId) throws LeaveTournamentNotFoundException,LeaveTournamentNotMemberException,LeaveTournamentOnlyAdminException {
		Team teamFound = Team.get(teamId)

		if (teamFound!=null) {
			def joinedTournament = teamFound.tournament
			// See if its one of his teams
			if (teamFound.player.id==player.id) {

				// See if it is an ADMIN
				if (teamFound.teamRole==TeamRole.ADMIN) {
					//See if it is the only admin
					def numberOfAdmins =Team.where {
						tournament.id == joinedTournament.id &&
								teamRole == TeamRole.ADMIN }.count()

					if (numberOfAdmins==1) {
						throw new LeaveTournamentOnlyAdminException (message:"Only one admin in the tournament", tournament: joinedTournament)
					}
				}

				joinedTournament.numberOfTeams = joinedTournament.numberOfTeams - 1
				if  (joinedTournament.status == TournamentStatus.FULL) {
					joinedTournament.status = TournamentStatus.ADMIN_CLOSED
				}
				
				//free all footballers that belongs to this team
				Footballer.executeUpdate("update Footballer f set f.team.id = null where f.team.id=:sqlTeamId", [sqlTeamId: teamFound.id] )

				joinedTournament.save()
				teamFound.delete()

				return joinedTournament
			}
			else {
				throw new LeaveTournamentNotMemberException (message:" Not tournament member", tournament: joinedTournament)
			}
		}
		else{
			throw new LeaveTournamentNotFoundException (message:"Team Not Found Exception", tournament: null)
		}
	}

	def Team show (int teamId) throws ShowTournamentNotFoundException{
		def team = Team.get(teamId)
		if (team!=null) {
			return team
		}
		else {
			throw new ShowTournamentNotFoundException(message:"Tournament not found Exception",tournament:null)
		}
	}

	def Tournament update(Tournament tournament) throws UpdateTournamentValidationException, grails.validation.ValidationException{

		if (tournament.validate()) {
			if (tournament.save()) {
				return tournament
			}
		}
		throw new UpdateTournamentValidationException(message:"Tournament update validation Exception",tournament:tournament)
	}


	//FIXME: Is this method neccesary ?
	public boolean isTournamentAdminMember (Player lamePlayer,Team lameTeam) {
		boolean member = this.isTournamentMember(lamePlayer,lameTeam)
		if (member) {
			return lameTeam.teamRole == TeamRole.ADMIN
		}
		return false
	}

	def boolean delete(Player player,Team deleteTeam) {

		if ((deleteTeam!=null) && (player!=null)) {
			def tournament = deleteTeam.tournament
			if (tournament!=null) {
				def securityFilters = new LameSecurityFilters()
				boolean isAdmin = securityFilters.isTournamentAdminMember(player, deleteTeam)
				if (isAdmin) {
					tournament.delete()
					return true
				}
				else {
					throw new DeleteTournamentNotAdminException (message: "Error deleting tournament", tournament:tournament)
				}
			}
		}
		throw new DeleteTournamentGenericException (message: "Generic error deleting tournament", tournament:null)
	}


	def List listFootballers (Player player,Team team) {
		if ((team!=null) && (player!=null)) {
			def tournament = team.tournament
			if (tournament!=null) {
				def securityFilters = new LameSecurityFilters()
				boolean isMember = securityFilters.isTournamentMember(player, team)
				if (isMember) {
					//because of lazy fetch
					def tournamentFound = Tournament.get(tournament.id)
					if (tournamentFound != null) {
						return tournamentFound.footballers
					}
					else {
						throw new ListFootballersNotFoundException (message: "No tournament was found", tournament:tournament)
					}

				}
				else {
					throw new ListFootballersNotMemberException (message: "Not Tournament Member Exception", tournament:tournament)
				}

			}
			throw new ListFootballersGenericException (message: "Generic error listing footballers", tournament:null)
		}
	}


	def List <Footballer> searchFootballerByName (String value,Tournament param_tournament) {
		// Look for Name
		def criteria = Footballer.createCriteria()
		def results = criteria.list () {
			and {
				realFootballer {
					ilike("name", "%$value%")
					order ("position")
				}
				tournament { idEq (param_tournament.id) }

			}
		}
	}

	def List <Footballer> listTeamFootballers (Team param_team) {
 
		def criteria = Footballer.createCriteria()
		def results = criteria.list () {
			team { idEq (param_team.id) }
		}
		return results
	}

	//TODO: previous step of offer
	def Footballer signUpFootballer (Long footballerId,Team team, Integer offerAmount) {
		def footballerFound = Footballer.get(footballerId)
		if  (footballerFound != null) {
			if (offerAmount>=footballerFound.realFootballer.price) {
				//not assigned yet
				if (footballerFound.team==null) {
					def teamFound = Team.get(team.id)
					//have enough money in balance
					if (teamFound.balance >= offerAmount) {
						//
						if (teamFound.tournament.id == footballerFound.tournament.id) {					
							//Discount money
							teamFound.money = teamFound.money - offerAmount
							
							//update balance
							teamFound.balance = teamFound.money
							
							//Assign footballer to the team							
							footballerFound.team=teamFound
							teamFound.save(failOnError: true)
							footballerFound.save(failOnError: true)
							return footballerFound
						}
						else {
							//TODO: throw FootballerNotBelongsToTournamentException
						}
					}
					else {
						//TODO: throw NotEnoughMoney
					}
				}
				else {
					//TODO: throw AlreadyAssignedException
				}

			}
			else {
				//TODO: throw OfferLowerThanPriceException
			}
		}
	}

	
	def serviceMethod() {
	}
}
