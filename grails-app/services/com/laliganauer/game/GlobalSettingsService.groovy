package com.laliganauer.game

import com.laliganauer.game.GlobalSettings;

class GlobalSettingsException extends RuntimeException {
	String message
	GlobalSettings globalSettings
}

//FIXME: Deberia ser singleton o clase estática
class GlobalSettingsService {
	private static final String tournament_max_teams_default = "tournament.max.teams.default"
	private static final String tournament_current_real_tournament = "tournament.current.real.tournament"
	private static final String tournament_current_real_matchweek = "tournament.current.real.matchweek"
	private static final String tournament_team_initial_amount = "tournament.team.initial.amount"


	/*private methods*/
	def private void setValue(String name, String value) throws GlobalSettingsException {
		def globalSettings = GlobalSettings.findByName(name)
		if (globalSettings!=null) {
			globalSettings.value= value
			globalSettings.save()
		}
		else {
			throw new GlobalSettingsException (message:"Param $name not found",globalSettings)
		}
	}

	def private GlobalSettings getValue(String name) throws GlobalSettingsException {
		def globalSettings = GlobalSettings.findByName(name)
		if (globalSettings!=null) {
			return globalSettings
		}
		else {
			throw new GlobalSettingsException (message:"Param $name not found",globalSettings:globalSettings)
		}
	}

	def void createParameter(String name, String value) throws GlobalSettingsException {
		def globalSettings = GlobalSettings.findByName(name)
		if (globalSettings==null) {
			globalSettings = new GlobalSettings(name:name,value:value)
			globalSettings.save()
		}
	}

	/*tournament max teams by default*/
	def  void createTournamentMaxTeamsDefault(Integer maxTeams) throws GlobalSettingsException {
		this.createParameter(GlobalSettingsService.tournament_max_teams_default,maxTeams.toString())
	}


	def  void setTournamentMaxTeamsDefault(Integer maxTeams) throws GlobalSettingsException {
		this.setValue(GlobalSettingsService.tournament_max_teams_default, maxTeams)
	}

	def  Integer getTournamentMaxTeamsDefault() throws GlobalSettingsException {

		try {
			def globalSettings = this.getValue(GlobalSettingsService.tournament_max_teams_default)
			return Integer.valueOf(globalSettings.value)
		}
		catch (NumberFormatException numberFormatException) {
			throw new GlobalSettingsException (message:numberFormatException.getMessage(),globalSettings)
		}

	}

	/*current real tournament*/
	def  void createCurrentRealTournament(String tournamentName) throws GlobalSettingsException {
		this.createParameter(GlobalSettingsService.tournament_current_real_tournament,tournamentName)
	}

	def  void setCurrentRealTournament (String tournamentName) throws GlobalSettingsException {
		this.setValue(GlobalSettingsService.tournament_current_real_tournament, tournamentName)
	}

	def  String getCurrentRealTournament() throws GlobalSettingsException {

		def globalSettings = this.getValue(GlobalSettingsService.tournament_current_real_tournament)
		return globalSettings.value

	}
	
	/*Current number of real matchweek*/
	def  void createCurrentRealMatchWeek(Integer currentMatchWeek) throws GlobalSettingsException {
		this.createParameter(GlobalSettingsService.tournament_current_real_matchweek,currentMatchWeek.toString())
	}


	def  void setCurrentRealMatchWeek(Integer currentMatchWeek) throws GlobalSettingsException {
		this.setValue(GlobalSettingsService.tournament_current_real_matchweek, currentMatchWeek)
	}

	def  Integer getCurrentRealMatchWeek() throws GlobalSettingsException {

		try {
			def globalSettings = this.getValue(GlobalSettingsService.tournament_current_real_matchweek)
			return Integer.valueOf(globalSettings.value)
		}
		catch (NumberFormatException numberFormatException) {
			throw new GlobalSettingsException (message:numberFormatException.getMessage(),globalSettings)
		}

	}



	/*Initial amount of money*/
	def  void createTeamInitialAmount(Integer initialAmount) throws GlobalSettingsException {
		this.createParameter(GlobalSettingsService.tournament_team_initial_amount,initialAmount.toString())
	}


	def  void setTeamInitialAmount(Integer initialAmount) throws GlobalSettingsException {
		this.setValue(GlobalSettingsService.tournament_team_initial_amount, initialAmount)
	}

	def  Integer getTeamInitialAmount() throws GlobalSettingsException {

		try {
			def globalSettings = this.getValue(GlobalSettingsService.tournament_team_initial_amount)
			return Integer.valueOf(globalSettings.value)
		}
		catch (NumberFormatException numberFormatException) {
			throw new GlobalSettingsException (message:numberFormatException.getMessage(),globalSettings)
		}

	}








	def serviceMethod() {
	}
}
