package com.laliganauer.utils

import grails.validation.ValidationErrors
import org.apache.commons.logging.Log
import org.springframework.validation.Errors
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError

public class Utils {

	public static void logErrors (Log log1,ValidationErrors errors, Exception exception) {

		if (errors.hasFieldErrors()) {
			errors.getFieldErrors().each { fieldError ->
				log1.info ("Field error [objectName=" + fieldError.getObjectName() + ", field=" + fieldError.getField() + ", rejectedValue="+ fieldError.getRejectedValue()+ ", code=" +fieldError.getCode()+"]")

			}
		}

		if (errors.hasGlobalErrors()) {
			errors.getGlobalErrors().each { globalError ->
				log1.error ("Global error [objectName=" + globalError.getObjectName() + ",code=" +globalError.getCode()+"]", exception)				
			}
		}
	}
}
