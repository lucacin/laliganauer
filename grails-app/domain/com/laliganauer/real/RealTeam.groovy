package com.laliganauer.real

import com.laliganauer.real.RealFootballer

class RealTeam {

	String name
	String stadiumName
	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


	static hasMany=[realFootballers:RealFootballer]
	
   static constraints = {
		name (blank:false, unique:true, maxSize: 50)
		stadiumName (nullable:true,maxSize: 50)
	}


	static scaffold = true


	String toString() {
		return "${name}"
	}
}
