package com.laliganauer.real

import java.util.Date;

class RealMatch {
	Date startTime

	RealTeam homeTeam
	RealTeam visitorTeam

	Integer homeGoals
	Integer visitorGoals

		
	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


    static constraints = {
		homeTeam (blank:false)
		visitorTeam (blank:false)
		homeGoals (blank:false)
		visitorGoals (blank:false)
    }
}
