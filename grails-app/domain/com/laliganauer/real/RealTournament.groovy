package com.laliganauer.real


class RealTournament {

	String name
	
	Date startDate
	Date endDate

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


	
	static hasMany=[realMatchWeeks:RealMatchWeek, realTeams:RealTeam]

   static constraints = {
		name (blank:false, unique:true)
		startDate (blank:false)
		endDate (blank: false)
	}

	static scaffold = true
}
