package com.laliganauer.real

import com.laliganauer.game.tournament.team.Lineup;

class RealMatchWeek {

	Integer number

	Date startDate
	Date endDate

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static belongsTo =[realTournament:RealTournament]

	static hasMany=[realMatches: RealMatch]

	static constraints = {
		number (blank:false)
		startDate (blank:false)
		endDate (blank: false)
	}

	static scaffold = true

	String toString() {
		return """("${this.number}","${this.startDate}")"""
	}
}
