package com.laliganauer.real

import java.util.Date;

enum Position {
	GOALKEEPER,DEFENDER,MIDFIELDER,STRIKER
}

class RealFootballer {
	String name
	byte[] photo
	Position position
	Integer price

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp
	
	static belongsTo = [realTeam: RealTeam] 
	
	static constraints = {
		name (blank:false)
		position (blank:false)
		photo (nullable:true)
		price (blank:false)
		realTeam(nullable:true)
	}

	String toString() {
		return "(Name=${name},Position=${position})" 
	}
}
