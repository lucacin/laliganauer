package com.laliganauer.game.tournament.team

import com.laliganauer.game.tournament.Footballer
import com.laliganauer.real.RealMatchWeek
import com.laliganauer.real.RealTournament
import com.laliganauer.real.Position

enum LineupStatus {
	NOT_SAVED,SAVED_OK,SAVED_INVALID
}


class Lineup {

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp



	Float points
	RealMatchWeek realMatchWeek
	LineupStatus lineupStatus
	String currentFormation

	static belongsTo =[team:Team]
	static hasMany = [lineupFootballers: LineupFootballer]


	static final validFormations = [
		['gk':1, 'df':3, 'md':4, 'st':3],
		['gk':1, 'df':3, 'md':5, 'st':2],
		['gk':1, 'df':4, 'md':3, 'st':3],
		['gk':1, 'df':4, 'md':4, 'st':2],
		['gk':1, 'df':4, 'md':5, 'st':1],
		['gk':1, 'df':5, 'md':3, 'st':2],
		['gk':1, 'df':5, 'md':4, 'st':1]
	]

	static constraints = {

	}

	static scaffold = true

	public boolean validateAndUpdateFormation() {

		def goalkeepers = [(LineupFootballerStatus.PERMANENT):[],(LineupFootballerStatus.RESERVE):[]]
		def defenders = [(LineupFootballerStatus.PERMANENT):[],(LineupFootballerStatus.RESERVE):[]]
		def midfielders = [(LineupFootballerStatus.PERMANENT):[],(LineupFootballerStatus.RESERVE):[]]
		def strikers = [(LineupFootballerStatus.PERMANENT):[],(LineupFootballerStatus.RESERVE):[]]

		for (lineupFb in lineupFootballers) {
			if ((lineupFb.lineupFootballerStatus == LineupFootballerStatus.PERMANENT) || (lineupFb.lineupFootballerStatus == LineupFootballerStatus.RESERVE)) {

				switch (lineupFb.realFootballer.position) {
					case Position.GOALKEEPER:
						goalkeepers[(lineupFb.lineupFootballerStatus)].add(lineupFb)
						break
					case Position.DEFENDER:
						defenders[(lineupFb.lineupFootballerStatus)].add(lineupFb)
						break
					case Position.MIDFIELDER:
						midfielders[(lineupFb.lineupFootballerStatus)].add(lineupFb)
						break
					case Position.STRIKER:
						strikers[(lineupFb.lineupFootballerStatus)].add(lineupFb)
						break
					default:
					//TODO: throw GeneralException
						break
				}
			}
		}
		def permanents = [
			'gk':goalkeepers[LineupFootballerStatus.PERMANENT].size(),
			'df':defenders[LineupFootballerStatus.PERMANENT].size(),
			'md':midfielders[LineupFootballerStatus.PERMANENT].size(),
			'st':strikers[LineupFootballerStatus.PERMANENT].size()
		]

		def reserves = [
			'gk':goalkeepers[LineupFootballerStatus.RESERVE].size(),
			'df':defenders[LineupFootballerStatus.RESERVE].size(),
			'md':midfielders[LineupFootballerStatus.RESERVE].size(),
			'st':strikers[LineupFootballerStatus.RESERVE].size()
		]
		
		def formation = ['goalkeepers':['permanents':permanents['gk'],'reserves':reserves['gk']],
			'defenders':['permanents':permanents['df'],'reserves':reserves['df']],
			'midfielders':['permanents':permanents['md'],'reserves':reserves['md']],
			'strikers':['permanents':permanents['st'],'reserves':reserves['st']]
		]


		this.currentFormation=formation.toString()
		if (permanents in Lineup.validFormations) {
			if ((reserves['gk']<=1) && (reserves['df']<=1) && (reserves['md']<=1) && (reserves['st']<=1)) {
				this.lineupStatus=LineupStatus.SAVED_OK
				return true
			}
			else {
				this.lineupStatus=LineupStatus.SAVED_INVALID
				return false
			}
		}
		else {
			this.lineupStatus=LineupStatus.SAVED_INVALID
			return false
		}
	}

	public boolean addLineupFootballer (LineupFootballer lineupFootballer_param) {
		if (lineupFootballer_param != null) {
			if ((lineupFootballer_param.lineupFootballerStatus==LineupFootballerStatus.PERMANENT) || (lineupFootballer_param.lineupFootballerStatus==LineupFootballerStatus.RESERVE)) {
				this.addToLineupFootballers(lineupFootballer_param)
				return true
			}
		}
		return false
	}
}
