package com.laliganauer.game.tournament.team

import com.laliganauer.real.Position
import com.laliganauer.real.RealFootballer

enum LineupFootballerStatus {
	PERMANENT,RESERVE,NOT_SELECTED
}

class LineupFootballer {
	RealFootballer realFootballer
	LineupFootballerStatus lineupFootballerStatus
	Position currentPosition
	Float points

    static constraints = {
		realFootballer (nullable:false)
		lineupFootballerStatus  (nullable:false)
		currentPosition (nullable:false)
    }
	
	static belongsTo = Lineup
	
	String toString () {
		return """(RealFootballer="$realFootballer",LineupStatus="lineupFootballerStatus",currentPosition="$currentPosition",points="$points" )"""
	}
}
