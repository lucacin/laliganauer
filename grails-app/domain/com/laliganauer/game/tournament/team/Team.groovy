package com.laliganauer.game.tournament.team

import com.laliganauer.game.tournament.Footballer;
import com.laliganauer.game.tournament.Tournament
import com.laliganauer.game.player.Player

public enum TeamRole {
	ADMIN,PLAYER
}

public enum FootballerStatusInTeam {
	PERMANENT,RESERVE, FREE
}

class Team {

	String name
	Integer money //real money
	Integer balance //money taking account offers 
	TeamRole teamRole


	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp


	//FIXME: belongsTo implies delete in cascade
	static belongsTo =[tournament:Tournament,player:Player]
	
	static hasMany = [lineups:Lineup]

	static constraints = {
		name(blank:false,unique:false,size: 5..25)
		teamRole(blank:false)
		tournament (unique: 'player') // Only one player per tournament
	}

	//static scaffold = true

	String toString() {
		return "${name}"		
	}

}
