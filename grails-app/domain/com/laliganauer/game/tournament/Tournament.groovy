package com.laliganauer.game.tournament

import java.util.UUID

import com.laliganauer.game.tournament.team.Team

public enum TournamentStatus {
	OPENED,FULL,ADMIN_CLOSED
}

class Tournament {

	String reference //it's an UUID
	String name
	String description

	TournamentSettings tournamentSettings
	
	TournamentStatus status
	Integer numberOfTeams
	
	List footballers

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static hasMany=[teams:Team, footballers:Footballer]

	static constraints = {
		reference (blank: false, unique: true, maxSize: 50) //random identifier
		name (blank: false, size: 5..30) // name is not unique, only id
		description (nullable:true, maxSize: 100)
		status (nullable:false,defaultValue: TournamentStatus.OPENED)
		tournamentSettings (nullable:false)
		numberOfTeams (nullable:false, defaultValue:0)
	}

	static scaffold = true

	@Override
	public String toString() {
		return """Tournament [reference="$reference", name="$name", description="$description", tournamentSettings="$tournamentSettings"]"""
	}
	
	def public boolean isInAJoinableStatus () {
		return this.status == TournamentStatus.OPENED
	}
	
	def public boolean isInAEditableStatus () {
		return this.status != TournamentStatus.FULL
	}

	def public List<TournamentStatus> getEditableStatuses () {
		return [TournamentStatus.OPENED,TournamentStatus.ADMIN_CLOSED]
	}
	
	def public List<TournamentStatus> getNoEditableStatuses () {
		return [TournamentStatus.FULL]
	}

	public void trim(){
		if (this.name!=null) {
			this.name=this.name.trim()
		}
		if (this.description!=null) {
			this.description=this.description.trim()
		}
	}
	
	public void setRandomId() {
		this.reference = UUID.randomUUID() 
	}
}
