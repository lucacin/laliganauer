package com.laliganauer.game.tournament


import com.laliganauer.real.RealFootballer
import com.laliganauer.game.tournament.team.Team



class Footballer {

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static belongsTo = [realFootballer:RealFootballer, tournament: Tournament,team: Team ]

	static constraints = {
		realFootballer (nullable:false)
		tournament (nullable:false)
		team (nullable:true,blank:false)		
	}

	static scaffold = true
	
	

	String toString() {
		return "${realFootballer} in ${team}"
	}

}
