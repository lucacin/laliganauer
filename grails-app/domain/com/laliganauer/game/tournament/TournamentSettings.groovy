package com.laliganauer.game.tournament

import java.util.Date;


class TournamentSettings {

	Integer max_teams // max teams per tournament

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static belongsTo = Tournament

	static constraints = {
		max_teams (nullable:false, range:5..15)
	}

	@Override
	public String toString() {
		return """TournamentSettings [max_players="$max_teams"]"""
	}
}
