package com.laliganauer.game


import java.util.Date

class GlobalSettings {

	String name // property name
	String value // property value

	Date dateCreated // Grails automatically set this value with created timestamp
	Date lastUpdated // Grails automatically set this value with last update timestamp

	static constraints = {
		name(blank:false,unique:true)
		value(blank:false)
	}

	@Override
	public String toString() {
		return """GlobalSettings [name="$name", value="$value"]"""
	}	
}
