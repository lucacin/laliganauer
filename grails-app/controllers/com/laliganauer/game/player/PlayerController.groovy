package com.laliganauer.game.player

import com.grailsinaction.LameSecurityFilters
import com.laliganauer.game.player.Player
import com.laliganauer.utils.Utils


import grails.validation.ValidationException

import grails.validation.Validateable

@Validateable
class RegisterCommand {

	String login
	String email

	String password
	String confirmPassword

	static constraints = {
		importFrom Player

		password (blank:false, size: 6..12,password:true)
		confirmPassword (blank:false, password:true, validator: { value,pc ->
			return value == pc.password
		})
	}

	@Override
	public String toString() {
		return "RegisterCommand [login=" + login + ", email=" + email + ", password=" + password + ", confirmPassword=" + confirmPassword + "]"
	}

	public void trim(){
		if (this.login!=null) {
			this.login=this.login.trim()
		}
		if (this.email!=null) {
			this.email=this.email.trim()
		}
		if (this.password!=null) {
			this.password=this.password.trim()
		}
		if (this.confirmPassword!=null) {
			this.confirmPassword=this.confirmPassword.trim()
		}
	}
}



@Validateable
class ForgotPasswordCommand {

	String login
	String email
	String password
	String confirmPassword


	//same constraints as Player, but I can't use importFrom because of the unique constraint of each one
	static constraints = {
		login(blank:false,size: 4..12)
		email(blank:false,email:true)
		importFrom RegisterCommand, include: [
			"password",
			"confirmPassword"
		]
	}

	@Override
	public String toString() {
		return "ForgotPasswordCommand [login=" + login + ", email=" + email + ", password=" + password + ", confirmPassword="+ confirmPassword + "]"
	}

	public void trim(){
		if (this.login!=null) {
			this.login=this.login.trim()
		}
		if (this.email!=null) {
			this.email=this.email.trim()
		}
		if (this.password!=null) {
			this.password=this.password.trim()
		}
		if (this.confirmPassword!=null) {
			this.confirmPassword=this.confirmPassword.trim()
		}
	}
}

@Validateable
class LoginCommand {
	String login
	String password

	static constraints = { importFrom RegisterCommand }

	@Override
	public String toString() {
		return "LoginCommand [login=" + login + ", password=" + password + "]";
	}

	public void trim(){
		if (this.login!=null) {
			this.login=this.login.trim()
		}
		if (this.password!=null) {
			this.password=this.password.trim()
		}
	}
}



class PlayerController {

	//automatically injects postService into the controller
	def playerService

	static scaffold = true

	def index() {

	}

	def login (LoginCommand loginCommand) {
		log.debug "Los params son ${params} y el command es ${loginCommand}"

		if (loginCommand.login!=null) {
			try {
				//limpio espacios en blanco
				loginCommand.trim()
				def player = playerService.login(loginCommand)
				def securityFilters = new LameSecurityFilters()
				securityFilters.setAuthentication(session,player)
				log.info "$player successfully logged in"
				redirect (uri: '/player/home')
			}
			catch (ValidationException validationException){
				Utils.logErrors(log,validationException.getErrors(),validationException)

				return [playerView: loginCommand]
			}
		}
	}

	def register (RegisterCommand registerCommand) {
		log.debug "Los params son ${params} y el command es ${registerCommand}"

		if (registerCommand.login!=null) {
			try {
				registerCommand.trim()
				def player=playerService.register(registerCommand)
				log.info "New $player successfully registered"
				flash.OKMessage = message(code: "register.OK", args: [player.login])
				redirect (uri: '/')
			}

			catch (ValidationException validationException){
				Utils.logErrors(log,validationException.getErrors(),validationException)

				return [playerView: registerCommand, errors:validationException.errors]
			}
		}
	}

	def forgotPassword (ForgotPasswordCommand forgotPasswordCommand) {

		log.debug "Los params son ${params} y el command es ${forgotPasswordCommand}"

		if (forgotPasswordCommand.login!=null) {
			forgotPasswordCommand.trim()

			try {
				Player player = null
				switch ( params.forgotPasswordSelected ) {
					case "login":
						player=playerService.forgotPasswordByLogin(forgotPasswordCommand)
						break
					case "email":
						player=playerService.forgotPasswordByEmail(forgotPasswordCommand)
						break
				}
				log.info "New password was requested by $player"
				flash.OKMessage = message(code: "forgotPassword.OK", args: [player.login])
				redirect (uri: '/')
			}
			catch (ValidationException validationException){
				Utils.logErrors(log,validationException.getErrors(),validationException)
				return [forgotPasswordView: forgotPasswordCommand, errors:validationException.errors]
			}
		}
	}


	def home() {
	}


	def logout () {
		def login = session.player.login

		session.invalidate()
		flash.OKMessage = message(code: "player.logout.ok", args: [login])
		log.info "Player $login successfully logged out"
		redirect (uri: '/')
	}
}
