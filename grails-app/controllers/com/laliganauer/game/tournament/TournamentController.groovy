package com.laliganauer.game.tournament

import grails.validation.ValidationException

import com.grailsinaction.LameSecurityFilters
import com.laliganauer.game.tournament.CreateTournamentCommandException
import com.laliganauer.game.tournament.DeleteTournamentGenericException
import com.laliganauer.game.tournament.DeleteTournamentNotAdminException
import com.laliganauer.game.tournament.JoinTournamentAlreadyRegisteredException
import com.laliganauer.game.tournament.JoinTournamentClosedException
import com.laliganauer.game.tournament.JoinTournamentFullException
import com.laliganauer.game.tournament.JoinTournamentNotFoundException
import com.laliganauer.game.tournament.LeaveTournamentNotFoundException
import com.laliganauer.game.tournament.LeaveTournamentNotMemberException
import com.laliganauer.game.tournament.LeaveTournamentOnlyAdminException
import com.laliganauer.game.tournament.ShowTournamentNotFoundException
import com.laliganauer.game.tournament.TeamException
import com.laliganauer.game.tournament.Tournament
import com.laliganauer.game.tournament.TournamentException
import com.laliganauer.game.tournament.UpdateTournamentValidationException
import com.laliganauer.game.tournament.ValidateTeamException
import com.laliganauer.game.tournament.team.Team;
import com.laliganauer.game.tournament.team.TeamService

import grails.validation.Validateable

@Validateable
class CreateTournamentCommand {
	String name
	String description

	static constraints = { importFrom Tournament }

	@Override
	public String toString() {
		return """createTournamentCommand [name="$name", description="$description"]"""
	}

	public void trim(){
		if (this.name!=null) {
			this.name=this.name.trim()
		}
		if (this.description!=null) {
			this.description=this.description.trim()
		}
	}
}



class TournamentController {

	//automatically injects tournamentService into the controller
	def tournamentService

	//static scaffold = Tournament



	def create (CreateTournamentCommand createTournamentCommand) {
		log.debug "Los params son ${params}"
		log.debug "El command es ${createTournamentCommand}"


		if (createTournamentCommand.name!=null) {
			try {
				createTournamentCommand.trim()
				def tournament = tournamentService.createTournamentAndAssociatedTeam(createTournamentCommand,session.player,message(code: "team.new.default.name"))
				flash.OKMessage = message(code: "tournament.createTournament.ok", args: [tournament.name, tournament.reference])
				redirect (uri: '/tournament/listByPlayer')
			}
			catch (ValidationException validationException) {
				if (validationException.errors.hasGlobalErrors()) {
					log.error ("Global errors exception",validationException)
				}
				else {
					log.info (validationException.getMessage())
				}
				return [tournament: createTournamentCommand, errors:validationException.errors ]
			}
			catch (CreateTournamentCommandException createTournamentCommandException) {
				if (createTournamentCommandException.createTournamentCommand.errors.hasGlobalErrors()) {
					log.error ("Global errors exception",createTournamentCommandException)
				}
				else {
					log.info (createTournamentCommandException.getMessage())
				}
				return [tournament: createTournamentCommand, errors: createTournamentCommandException.createTournamentCommand.errors ]
			}
		}
	}

	def listByPlayer() {
		log.debug "Los params de listByPlayer son ${params}"
		def teamService = new TeamService()
		def teams = teamService.listByPlayer(session.player)
		return [teamsView: teams]
	}

	def search () {
		log.debug "Los params en search son ${params}"


		// Pagination
		//TODO: This value maxResultsPerPage better if it is recovered from GlobalSettings DB
		int maxResultsPerPage = 5
		int offset = 0
		if ((params.max!=null) && (params.offset!=null)) {
			try {
				offset = Integer.parseInt(params.offset)
			}
			catch (java.lang.NumberFormatException exception) {
				log.warn (exception.getMessage())
				log.debug (exception.printStackTrace())
				offset=0
			}
		}

		if (params.searchText != null) {

			String searchText= params.searchText.trim()
			def tournamentByReference = tournamentService.searchByReference(searchText)
			log.debug "By reference: $tournamentByReference"
			def tournamentsByName = tournamentService.searchByNamePaginate(searchText, maxResultsPerPage, offset)
			log.debug "By name: $tournamentsByName"
			return [searchText:searchText,tournamentByReference:tournamentByReference,tournamentsByName:tournamentsByName,tournamentsByNameCount: tournamentsByName.totalCount,maxResultsPerPage:maxResultsPerPage]
		}
	}

	def join() {
		log.debug "Los params de join son ${params}"

		if (params.tournamentId!=null) {
			try {
				int tournamentId = Integer.parseInt(params.tournamentId)
				def newTeamName = message(code: "team.new.default.name")
				def team = tournamentService.join(session.player, tournamentId, newTeamName)

				flash.OKMessage = message(code: "tournament.join.result.ok", args: [team.tournament.name])

			}
			catch (java.lang.NumberFormatException exception) {
				log.warn (exception.getMessage())
				log.debug (exception.printStackTrace())
				flash.KOMessage = message(code: 'tournament.join.error.tournament.not.found')

			}
			catch (JoinTournamentNotFoundException tournamentNotFoundException) {
				flash.KOMessage = message(code: 'tournament.join.error.tournament.not.found')
			}
			catch (JoinTournamentClosedException closedException) {
				flash.KOMessage = message(code: 'tournament.join.error.tournament.closed',args: [closedException.tournament.name])
			}
			catch (JoinTournamentFullException fullException) {
				flash.KOMessage = message(code: 'tournament.join.error.tournament.full',args: [closedException.tournament.name])
			}
			catch (JoinTournamentAlreadyRegisteredException alreadyRegisteredException) {
				flash.KOMessage = message(code: 'tournament.join.error.already.registered',args: [alreadyRegisteredException.tournament.name])
			}
			catch (ValidateTeamException validateTeamException) {
				flash.KOMessage = message(code: 'tournament.join.error.team.general',args: [validateTeamException.team.tournament.name])
			}
			catch (TeamException teamException) {
				flash.KOMessage = message(code: 'tournament.join.error.team.general',args: [teamException.team.tournament.name])
			}
			catch (TournamentException tournamentException) {
				flash.KOMessage = message(code: 'tournament.join.error.team.general',args: [tournamentException.tournament.name])
			}
			redirect (uri: '/tournament/listByPlayer')
		}
	}

	def leave() {
		log.debug "Los params de leave son ${params}"
		if (params.teamId!=null) {
			try {
				int teamId = Integer.parseInt(params.teamId)
				def tournament = tournamentService.leave(session.player,teamId)

				flash.OKMessage = message(code: "tournament.leave.result.ok", args: [tournament.name])

			}
			catch (java.lang.NumberFormatException exception) {
				log.warn (exception.getMessage())
				log.debug (exception.printStackTrace())
				flash.KOMessage = message(code: 'tournament.leave.error.tournament.not.found')
			}
			catch (LeaveTournamentNotFoundException notFoundException) {
				flash.KOMessage = message(code: 'tournament.leave.error.tournament.not.found')
			}
			catch (LeaveTournamentNotMemberException notMemberException) {
				flash.KOMessage = message(code: 'tournament.leave.error.not.member',args: [notMemberException.tournament.name])
			}
			catch (LeaveTournamentOnlyAdminException onlyAdminException) {
				flash.KOMessage = message(code: 'tournament.leave.error.only.admin',args: [onlyAdminException.tournament.name])
			}
			redirect (uri: '/tournament/listByPlayer')
		}
	}

	def show() {
		if (params.teamId!=null) {
			try {
				int teamId = Integer.parseInt(params.teamId)
				def team = tournamentService.show(teamId)
				session.team=team
				def securityFilters = new LameSecurityFilters()
				boolean isAdmin = securityFilters.isTournamentAdminMember(session.player, session.team)
				boolean isMember = securityFilters.isTournamentMember(session.player, session.team)
				return [team: team, isAdmin: isAdmin, isMember: isMember]
			}
			catch (java.lang.NumberFormatException exception) {
				log.warn (exception.getMessage())
				log.debug (exception.printStackTrace())
				flash.KOMessage = message(code: 'tournament.show.error.tournament.not.found')
				redirect (uri: '/tournament/listByPlayer')
			}
			catch (ShowTournamentNotFoundException notFoundException) {
				flash.KOMessage = message(code: 'tournament.show.error.tournament.not.found')
				redirect (uri: '/tournament/listByPlayer')
			}
		}
	}

	def showSettings() {
		if (params!=null) {
			log.debug "Los params de edit son ${params}"
			def tournament = session.team.tournament?.attach()
			def securityFilters = new LameSecurityFilters()
			boolean isAdmin = securityFilters.isTournamentAdminMember(session.player, session.team)
			return [tournament: tournament, isAdmin:isAdmin]
		}
	}

	def update() {
		if (params!=null) {
			try {
				log.debug "Los params de update son ${params}"
				def tournament = session.team.tournament?.attach()
				if (tournament!=null) {
					tournament.properties['name', 'description', 'status'] = params
					tournament.trim()
					tournamentService.update(tournament)
					flash.OKMessage = message(code: 'update.tournament.result.ok')
					redirect (uri: "/tournament/show?teamId=$session.team.id")
				}
			}catch (UpdateTournamentValidationException updateTournamentValidationException){
				flash.KOMessage = message(code: 'update.tournament.result.ko')
				redirect (uri: '/tournament/showSettings')
			}
			catch (grails.validation.ValidationException validationSaveException) {
				log.error ("Update Exception",validationSaveException)
				flash.KOMessage = message(code: 'update.tournament.result.ko')
				redirect (uri: '/tournament/showSettings')
			}

		}
	}

	def delete() {
		log.debug "Los params de edit son ${params}"
		if (params!=null) {
			try {
				if (session.team!=null) {
					if (session.team.tournament!=null) {
						def tournamentName = session.team.tournament.name
						tournamentService.delete(session.player,session.team)
						flash.OKMessage = message(code: 'delete.tournament.result.ok',args: [tournamentName])
						session.removeAttribute('team')
						redirect (uri: '/tournament/listByPlayer')
						return
					}
				}
				flash.KOMessage = message(code: 'delete.tournament.result.ko')
				redirect (uri: '/tournament/listByPlayer')
			}
			catch (DeleteTournamentNotAdminException  deleteTournamentNotAdminException) {
				flash.KOMessage = message(code: 'error.notadmin.player')
				redirect (uri: '/tournament/listByPlayer')
			}
			catch (DeleteTournamentGenericException  deleteTournamentGenericException) {
				flash.KOMessage = message(code: 'delete.tournament.result.ko')
				redirect (uri: '/tournament/listByPlayer')
			}

		}
	}

	def listFootballers() {
		log.debug "Los params de listFootballers son ${params}"

		if (params!=null) {
			if (session.team!=null) {
				if (session.team.tournament!=null) {
					def footballers =tournamentService.listFootballers(session.player,session.team)
					return [footballersView: footballers, searchText:""]

				}
				//TODO: Implement control of exceptions
			}
		}
	}

	def listTeamFootballers() {
		log.debug "Los params de listTeamFootballers son ${params}"

		if (params!=null) {
			if (session.team!=null) {
				if (session.team.tournament!=null) {
					def footballers =tournamentService.listTeamFootballers(session.team)
					def sellButtonLabel = message(code: 'button.sell.footballer.value')
					return [footballersView: footballers, sellButtonLabelView:sellButtonLabel]

				}
				//TODO: Implement control of exceptions
			}
		}
	}

	//search footballer by name
	def searchFootballer() {
		log.debug "Los params en searchFootballer son ${params}"

		// Pagination
		//TODO: This value maxResultsPerPage better if it is recovered from GlobalSettings DB
		int  maxResultsPerPage = 5
		int offset = 0
		if ((params.max!=null) && (params.offset!=null)) {
			try {
				offset = Integer.parseInt(params.offset)
			}
			catch (java.lang.NumberFormatException exception) {
				log.warn (exception.getMessage())
				log.debug (exception.printStackTrace())
				offset=0
			}
		}

		if (params.searchText != null) {
			String searchText= params.searchText.trim()

			Tournament tournament  = session.team.tournament
			def footballersByName = tournamentService.searchFootballerByName(searchText,tournament)
			render (view: "listFootballers",model: [footballersView: footballersByName,searchText: searchText])
		}
	}

	def signUpFootballer () {
		log.debug "Los params en signUpFootballer son ${params}"


		if ((params.footballerId!=null) && (params.offerAmount!=null)) {
			try {
				def offerAmount = Integer.parseInt(params.offerAmount)
				def footballerId = Long.parseLong (params.footballerId)
				Team team = session.team
				Footballer signedUpFootballer = tournamentService.signUpFootballer (footballerId,team,offerAmount)
				flash.OKMessage = message(code: 'footballer.signed.up.ok',args: [signedUpFootballer.realFootballer.name, offerAmount])
				redirect (uri: '/tournament/listTeamFootballers') 				
			}
			catch (java.lang.NumberFormatException exception) {
				//TODO: Control that offer is an integer
			}
		}
	}


}
