package com.laliganauer.game.tournament.team

import com.laliganauer.real.Position
import com.laliganauer.game.tournament.team.Lineup
import com.laliganauer.game.tournament.team.LineupStatus


class SaveLineupCommand {
	Long realFootballerId
	LineupFootballerStatus status

	static constraints = {
		realFootballerId (blank:false)
		status (blank:false)
	}
}

class TeamController {

	def teamService


	def showLineup () {
		log.debug "params de showLineup ${params}"
		if (params!=null) {
			def lineupFootballers = teamService.showLineup(session.player, session.team)

			def goalkeepers = []
			def defenders = []
			def midfielders = []
			def strikers = []

			def subtitleArgs = [
				lineupFootballers['currentRealTournamentName'],
				lineupFootballers['currentRealMatchWeek']]
			def permanentLabel = message(code: "radiogroup.permanent.label")
			def reserveLabel = message(code: "radiogroup.reserve.label")
			def notSelectedLabel = message(code: "radiogroup.notSelected.label")
			def formationArgs = [lineupFootballers['formation']]

			def radioGroupLabels = [
				permanentLabel,
				reserveLabel,
				notSelectedLabel
			]
			def radioGroupValues = [
				LineupFootballerStatus.PERMANENT,
				LineupFootballerStatus.RESERVE,
				LineupFootballerStatus.NOT_SELECTED]
			for (iter in lineupFootballers['resultFootballersList']) {
				switch (iter.currentPosition) {
					case Position.GOALKEEPER:
						goalkeepers.add(iter)
						break
					case Position.DEFENDER:
						defenders.add(iter)
						break
					case Position.MIDFIELDER:
						midfielders.add(iter)
						break
					case Position.STRIKER:
						strikers.add(iter)
						break
					default:
					//TODO: Control a position that do not exist (error)
						break
				}
			}

			return [goalkeepersView: goalkeepers,defendersView: defenders,midfieldersView: midfielders,strikersView: strikers, lineupRealTournamentView: lineupFootballers['currentRealTournamentName'],lineupMatchWeekView: lineupFootballers['currentRealMatchWeek'],subtitleArgsView:subtitleArgs, lineupStatusView:lineupFootballers['lineupStatus'], lineupFormationView: formationArgs,rgLabelsView: radioGroupLabels, rgValuesView:radioGroupValues]
		}
	}

	def saveLineup () {
		log.debug "params de showLineup ${params}"
		if (params!=null) {
			def footballerIds = []
			footballerIds.addAll(params.footballerIds)

			def lineupFootballerCommandList = []

			for (fb in footballerIds) {
				def lineupFootballerCommand = new SaveLineupCommand(realFootballerId: Long.valueOf(fb),status: params[fb])
				lineupFootballerCommandList.add(lineupFootballerCommand)
			}

			def lineup=teamService.saveLineup(session.team,params.realTournament, Integer.valueOf(params.matchWeek), lineupFootballerCommandList)
			log.info "Lineup successfully registered ${lineup}"

			//TODO: KO messages (exception control)
			redirect (uri: '/team/showLineup')
		}
	}
	
	def sellFootballer () {
		log.debug "params de sellFootballer ${params}"
		if (params!=null) {
			def footballerId = Long.parseLong(params.footballerId)			

			def mapResult=teamService.sellFootballer(session.player, session.team, footballerId)
			def footballer = mapResult['footballer']
			flash.OKMessage = message (code:'footballer.sold.ok',args:[footballer])
			if (mapResult['footballerInLineup']) {				
				log.info "Footballer ${footballer} sold. Footballer was assigned to current lineup"
				redirect (uri: '/team/showLineup')
			}
			else {
				log.info "Footballer ${footballer} sold."
//				render (view: "listFootballers",model: [footballersView: footballersByName,searchText: searchText])
				redirect (uri: '/tournament/listTeamFootballers')								
			}
			

			//TODO: KO messages (exception control)
			
		}
	}

}
