package com.laliganauer.real

import com.laliganauer.real.RealTeam;

class RealTeamController {

       def scaffold = RealTeam
	
	def getTeam()
	{
		def team = new RealTeam()
		team=RealTeam.get(params.id)
		render team.getName()
	}
}
